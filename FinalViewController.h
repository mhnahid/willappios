//
//  FinalViewController.h
//  MakeWill
//
//  Created by Cor2Tect on 4/11/16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

#import "DisplaySignatureViewController.h"
#import "MakeSignController.h"


@interface FinalViewController : UIViewController<UIAlertViewDelegate,UIImagePickerControllerDelegate> //<UIApplicationDelegate>
 - (IBAction)btnPress:(id)sender;

@property(strong ,nonatomic)IBOutlet UIStoryboard *mainStoryboard;
@property(strong ,nonatomic)IBOutlet UIViewController *vc;

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong, nonatomic) MakeSignController *myViewController;
@property (strong, nonatomic) IBOutlet UIButton *btnSign;

@property (strong, nonatomic) IBOutlet UITextView *tvUserDetails;
@property (strong, nonatomic) IBOutlet UITextView *tvNameWithDate;

@property (strong, nonatomic) IBOutlet UIView *uvVideoView;
@property (strong, nonatomic) IBOutlet UIButton *btnShowRecorder;

@property (strong, nonatomic) MPMoviePlayerController *videoController;

@property (strong, nonatomic) IBOutlet UIButton *btnVideoRecord;
@property (strong, nonatomic) IBOutlet UIButton *btnGoPayment;

- (IBAction)btnSignAction:(id)sender;
- (IBAction)btnShowRecorderAction:(id)sender;

- (IBAction)btnLoadPayment:(id)sender;

@end
