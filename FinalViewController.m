//
//  FinalViewController.m
//  MakeWill
//
//  Created by Cor2Tect on 4/11/16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

#import "FinalViewController.h"
#import "DataClass.h"
#import "UIView+Toast.h"

@interface FinalViewController ()

@property (strong, nonatomic) IBOutlet UIView *viewCustom;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UITextView *tvReviewData;
- (IBAction)btnAction:(id)sender;

@end

@implementation FinalViewController

-(void)setRoundBorderRecordGo
{
    
    self.btnGoPayment.frame=CGRectMake(self.btnGoPayment.frame.origin.x, self.btnGoPayment.frame.origin.y,self.btnGoPayment.frame.size.width, self.btnGoPayment.frame.size.height);
    
    self.btnGoPayment.layer.cornerRadius=8.0f;
    self.btnGoPayment.layer.masksToBounds=YES;
    self.btnGoPayment.layer.borderWidth=1.0f;
    self.btnGoPayment.layer.borderColor=[[self.btnGoPayment backgroundColor] CGColor];
    [self.scrollView addSubview:self.btnGoPayment];
    
    self.btnShowRecorder.frame=CGRectMake(self.btnShowRecorder.frame.origin.x, self.btnShowRecorder.frame.origin.y,self.btnShowRecorder.frame.size.width, self.btnShowRecorder.frame.size.height);
    
    self.btnShowRecorder.layer.cornerRadius=8.0f;
    self.btnShowRecorder.layer.masksToBounds=YES;
    self.btnShowRecorder.layer.borderWidth=1.0f;
    self.btnShowRecorder.layer.borderColor=[[self.btnShowRecorder backgroundColor] CGColor];
    [self.uvVideoView addSubview:self.btnShowRecorder];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setRoundBorderRecordGo];
   // [self.scrollView layoutIfNeeded];
    
    
    //atributed
    NSString * details=[NSString stringWithFormat:@"שם מלא: %@, תעודת זהות: %@, כתובת: %@",[DataClass getOwnerName],[DataClass getOwnerIdCardNo],[DataClass getOwnerAddress]];
    
    NSMutableAttributedString* aString =
    [[NSMutableAttributedString alloc] initWithString:details];
    
    [aString setAttributes:@{NSForegroundColorAttributeName:[UIColor blueColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:14],NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}  range:(NSRange){0,aString
        .length} ];
    
    NSMutableAttributedString* mainString =[[NSMutableAttributedString alloc] initWithString:@"לכן אני הח\"מ מר/גב:"];
    [mainString setAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0]} range:(NSRange){0,mainString.length}];
    
    [mainString appendAttributedString:[[NSAttributedString alloc] initWithAttributedString: aString]];
    
     NSMutableAttributedString* leftString =[[NSMutableAttributedString alloc] initWithString: @" , בהיותי בדעה צלולה, שפוי/ה ומיושב/ת בדעתי ומוכשר/ת מכל הבחינות הדרושות על פי הדת וחוקי מדינת ישראל, מצווה בזה מרצוני הטוב והחופשי וללא אונס הכרח ולחץ כדלקמן:"];
    
    [leftString setAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0]} range:(NSRange){0,leftString.length}];
    
    [mainString appendAttributedString:[[NSAttributedString alloc] initWithAttributedString: leftString]];

    //end
    
    
    self.tvUserDetails.attributedText=mainString;
    self.tvUserDetails.textAlignment=NSTextAlignmentRight;

    
    NSLocale* currentLocale = [NSLocale currentLocale];
    NSString *nameWithDate=[NSString stringWithFormat:@"ולראיה באתי על החתום : %@ \n ביום : %@ ",[DataClass getOwnerName], [[NSDate date] descriptionWithLocale:currentLocale]];
    NSMutableAttributedString * nameWithDateAttr=[[NSMutableAttributedString alloc] initWithString:nameWithDate];
    [nameWithDateAttr setAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:14.0]} range:(NSRange){0,nameWithDateAttr.length}];
    NSLog(@"%@",nameWithDate);
    self.tvNameWithDate.attributedText=nameWithDateAttr;
    self.tvNameWithDate.textAlignment=NSTextAlignmentRight;
    
    
    if([DataClass getImagePath])
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"MyFolder"];
        NSString *fileName = [filePath stringByAppendingPathComponent:
                              [NSString stringWithFormat:@"%@.png", [DataClass getPersonName]]];
        
        //get the contents of the image file into the image
        UIImage *signature = [UIImage imageWithContentsOfFile:fileName];
        //display our signature image
        [_btnSign setBackgroundImage:signature forState:(UIControlStateNormal)];
        [_btnSign setTitle:@"" forState:UIControlStateNormal];
        //mySignatureView.image = signature;
    }
    else
    {
        [_btnSign setTitle:@"Signature" forState:UIControlStateNormal];
        [_btnSign setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    }
    
    if ([DataClass getVideoPath]) {
        
        self.btnShowRecorder.hidden=YES;
        UIButton *btnBigRecoder=[[UIButton alloc] initWithFrame:CGRectMake(self.uvVideoView.frame.size.width/2+25, 10, self.uvVideoView.frame.size.width/2-25, self.uvVideoView.frame.size.width/2-25)];
       // [btnBigRecoder setTitle:@"+" forState:UIControlStateNormal];
        [btnBigRecoder addTarget:self
                     action:@selector(bigRecdAction)
           forControlEvents:UIControlEventTouchUpInside];
        
        UIImage *buttonImage = [UIImage imageNamed:@"reload.png"];
        
        [btnBigRecoder setBackgroundImage:buttonImage forState:UIControlStateNormal];
       // btnBigRecoder.backgroundColor=[UIColor lightGrayColor];
        
        [self.uvVideoView addSubview:btnBigRecoder];
        
        self.videoController = [[MPMoviePlayerController alloc] init];
        
        [self.videoController setContentURL:[DataClass getVideoPath]];
        [self.videoController.view setFrame:CGRectMake (0, 10.0f, self.uvVideoView.frame.size.width/2+20, self.uvVideoView.frame.size.height-10)];
        [self.uvVideoView addSubview:self.videoController.view];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(videoPlayBackDidFinish:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:self.videoController];
        [self.videoController play];
        
                }
    
    //self.tvReviewData.text=[[[DataClass getDataDic] allValues] componentsJoinedByString:@"\n"];
    NSString * reviewText=@"3.	הנני מצווה כי כל רכושי מכל סוג ומין שהוא, הן בנכסי דלא ניידי והן בנכסי דניידי, כספים, מזומנים, חשבונות בנק, תוכניות חיסכון, קופות גמל, קרנות השתלמות, ניירות ערך, תכשיטים, ביטוחים, בורסה, מניות בעלים, וכיוצ\"ב הנני מצווה בזה ל\n";
    
    
    if(![DataClass getSingleSuccessorStatus])
    {
        NSArray * keys=[[DataClass getDataDic] allKeys];

         NSString * show=@"";
        for(NSString* key in keys)
        {
            NSArray * stArr=[self stringToArray:key];
            
            NSString * subName=@"";
            if([stArr[1] isEqualToString:@"1"])
            {
                subName=@"Name";
            }
            else if([stArr[1] isEqualToString:@"2"])
            {
                subName=@"Branch";
            }
            else if([stArr[1] isEqualToString:@"3"])
            {
                subName=@"Account";
            }
            else if([stArr[1] isEqualToString:@"4"])
            {
                subName=@"Successor";
            }
            else if([stArr[1] isEqualToString:@"5"])
            {
                subName=@"Notes";
            }
            
            NSString * value=[[DataClass getDataDic] valueForKey:key];
            
            NSString * line=[NSString stringWithFormat:@"%@ : %@\n",subName, value];
            
            NSLog(@"made line : %@",line);
            
           reviewText=[reviewText stringByAppendingString:line];
           // NSLog(@"%@",show);

        }
        self.tvReviewData.text=reviewText;
        [DataClass setMailBody:reviewText];
        self.tvReviewData.scrollEnabled=YES;
        NSLog(@" fianl %@ %@",show,keys);

    }
    else
    {
        NSString * show=[reviewText stringByAppendingString:[DataClass getSingleSuccessorName]];
        [DataClass setMailBody:show];
        self.tvReviewData.text=show;
    }
  
    self.scrollView.contentSize=CGSizeMake(self.view.frame.size.width, 2550);
}

//string to array6
-(NSArray *)stringToArray :(NSString *)tString
{
    NSMutableArray *characters = [[NSMutableArray alloc] initWithCapacity:[tString length]];
    for (int i=0; i < [tString length]; i++) {
        NSString *ichar  = [NSString stringWithFormat:@"%c", [tString characterAtIndex:i]];
        [characters addObject:ichar];
    }
    //NSLog(@"%@",characters);
    return characters;
}

- (void)videoPlayBackDidFinish:(NSNotification *)notification {
    NSLog(@"play back finished on review");
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Signature view
- (IBAction)btnSignAction:(id)sender {
    
    [DataClass setPersonName:@"nahid"];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    //create the navigation controller and add the controllers view to the window
    _navigationController = [[UINavigationController alloc] init];
    [self.window addSubview:[self.navigationController view]];
    
    //check if the viewcontroller exists, otherwise create it
    if(self.myViewController == nil)
    {
        MakeSignController *inputView = [[MakeSignController alloc] init];
        self.myViewController = inputView;
    }
    
    //push the viewcontroller into the navigation view controller stack
    [self.navigationController pushViewController:self.myViewController animated:YES];
    
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
}


- (IBAction)btnAction:(id)sender {
    NSLog(@"button press");
    NSLog(@"Button pressed");
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
}

- (IBAction)btnPress:(id)sender {
        NSLog(@"button press");
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

}

// loading video recorder page
-(void)bigRecdAction
{
    _mainStoryboard =[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    _vc=[_mainStoryboard instantiateViewControllerWithIdentifier:@"videoRecorder"];
    
    [self presentViewController:_vc animated:true completion:nil];
}
- (IBAction)btnShowRecorderAction:(id)sender {
    _mainStoryboard =[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    _vc=[_mainStoryboard instantiateViewControllerWithIdentifier:@"videoRecorder"];
    
    [self presentViewController:_vc animated:true completion:nil];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1)
    {
        _mainStoryboard =[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        _vc=[_mainStoryboard instantiateViewControllerWithIdentifier:@"PaymentView"];
        
        [self presentViewController:_vc animated:true completion:nil];
    }
}

- (IBAction)btnLoadPayment:(id)sender {
    if([DataClass getImagePath])
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"האם אתה בטוח ?"
                                                        message:@"  "
                                                       delegate:self
                                              cancelButtonTitle:@" לא"
                              
                                              otherButtonTitles:@" כן", nil];
        alert.alertViewStyle = UIAlertViewStyleDefault;
        [alert show];
        
            }
    else{
        
        [self.view makeToast:@"אנא , לחתום ראשון" duration:2 position:CSToastPositionCenter];

        NSLog(@"Please  sign first");
    }
}
@end
