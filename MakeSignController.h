//
//  MyViewController.h
//  SignOnTouch
//
//  Created by Cor2Tect on 3/29/16.
//  Copyright © 2016 Cor2Tect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DisplaySignatureViewController.h"

@interface MakeSignController :UIViewController <UIAlertViewDelegate>

@property (nonatomic, strong) UIImageView *mySignatureImage;
@property (nonatomic, assign) CGPoint lastContactPoint1, lastContactPoint2, currentPoint;
@property (nonatomic, assign) CGRect imageFrame;
@property (nonatomic, assign) BOOL fingerMoved;
@property (nonatomic, assign) float navbarHeight;

@property (strong, nonatomic) DisplaySignatureViewController *displaySignatureViewController;


@end
