//
//  DisplaySignatureViewController.m
//  SignOnTouch
//
//  Created by Cor2Tect on 3/29/16.
//  Copyright © 2016 Cor2Tect. All rights reserved.
//

#import "DisplaySignatureViewController.h"

@interface DisplaySignatureViewController ()

@end

@implementation DisplaySignatureViewController

@synthesize personName;
@synthesize signedBy;
@synthesize mySignatureView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationItem setTitle:@"חתימת תצוגה"];
    
    //create a label to display the name of the person who signed the document
    CGRect myFrame = CGRectMake(10.0f, 0.0f, 300.0f, 30.0f);
    signedBy = [[UILabel alloc] initWithFrame:myFrame];
    signedBy.font = [UIFont boldSystemFontOfSize:16.0f];
    signedBy.textAlignment =  NSTextAlignmentLeft;
    [self.view addSubview:signedBy];
    
    //get reference to the navigation frame to calculate bar height
    CGRect navigationframe = [[self.navigationController navigationBar] frame];
    int navbarHeight = navigationframe.size.height;
    
    //frame for our signature image
    CGRect imageFrame = CGRectMake(self.view.frame.origin.x+10, 30,
                                   self.view.frame.size.width-20,
                                   self.view.frame.size.height-navbarHeight-30);
    
    //create an image view to display our signature image
    self.mySignatureView = [[UIImageView alloc] init];
    [self.mySignatureView setFrame:imageFrame];
    [self.mySignatureView setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:self.mySignatureView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    //who signed the document
    signedBy.text = [NSString stringWithFormat:@"נחתם על ידי: %@", personName];
    
    //create the path to our image file
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"MyFolder"];
    NSString *fileName = [filePath stringByAppendingPathComponent:
                          [NSString stringWithFormat:@"%@.png", personName]];
    
    //get the contents of the image file into the image
    UIImage *signature = [UIImage imageWithContentsOfFile:fileName];
    //display our signature image
    mySignatureView.image = signature;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
