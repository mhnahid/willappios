//
//  DisplaySignatureViewController.h
//  SignOnTouch
//
//  Created by Cor2Tect on 3/29/16.
//  Copyright © 2016 Cor2Tect. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DisplaySignatureViewController : UIViewController

@property (nonatomic, strong) NSString *personName;
@property (nonatomic, strong) UILabel *signedBy;
@property (nonatomic, strong) UIImageView *mySignatureView;


@end
