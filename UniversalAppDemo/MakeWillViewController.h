//
//  MakeWillViewController.h
//  MakeWill
//
//  Created by Cor2Tect on 4/10/16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExpandableTableView.h"

@interface MakeWillViewController :UIViewController <UITextFieldDelegate,UIAlertViewDelegate >
@property(strong ,nonatomic)IBOutlet UIStoryboard *mainStoryboard;
@property(strong ,nonatomic)IBOutlet UIViewController *vc;

@property (strong, nonatomic) UITapGestureRecognizer *tap;


@property (strong, nonatomic) IBOutlet ExpandableTableView *tblExpandable;
@property (strong, nonatomic) IBOutlet UIScrollView *makeWillScroll;

//owner info
@property (strong, nonatomic) IBOutlet UITextField *tfOwnerFullName;
@property (strong, nonatomic) IBOutlet UITextField *tfOwnerIdCardNo;
@property (strong, nonatomic) IBOutlet UITextField *tfOwnerAddress;
@property (strong, nonatomic) IBOutlet UITextField *tfOwnerPhoneNo;
@property (strong, nonatomic) IBOutlet UITextField *tfSingleSuccessor;

@property (strong, nonatomic) IBOutlet UITextField *tmp;
@property (strong, nonatomic) IBOutlet UITextField *activeField;


@property (strong, nonatomic) IBOutlet UITextView *tvSingle;

//buttons and check box
@property (strong, nonatomic) IBOutlet UISwitch *ckMultiple;
@property (strong, nonatomic) IBOutlet UISwitch *ckConscious;
@property (strong, nonatomic) IBOutlet UIButton *btnReviewWill;

- (IBAction)ckMultipleAction:(id)sender;
- (IBAction)btnReviewWillAction:(id)sender;
- (IBAction)ckConsciousAction:(id)sender;



@end
