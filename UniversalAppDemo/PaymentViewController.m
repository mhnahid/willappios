//
//  PaymentViewController.m
//  MakeWill
//
//  Created by Cor2Tect on 4/13/16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

#import "PaymentViewController.h"
#import "MailCore/mailcore.h"
#import <AddressBook/AddressBook.h>
//#import <JBDeviceOwner/JBDeviceOwner.h>
#import "DataClass.h"
@interface PaymentViewController ()<UITextFieldDelegate,UIAlertViewDelegate,NSURLConnectionDelegate>

@property (strong, nonatomic) UITapGestureRecognizer *tap;
@property (strong, nonatomic) IBOutlet UITextField *tmp;
@property (strong, nonatomic) IBOutlet UIScrollView *payScrollView;
@property (strong, nonatomic) NSMutableData *data;

@end

@implementation PaymentViewController

static Boolean  isPaid;
static NSString * usermail;

-(void)setRoundBorderPayment
{
    
    self.btnPayment.frame=CGRectMake(self.btnPayment.frame.origin.x, self.btnPayment.frame.origin.y,self.btnPayment.frame.size.width, self.btnPayment.frame.size.height);
    
    self.btnPayment.layer.cornerRadius=8.0f;
    self.btnPayment.layer.masksToBounds=YES;
    self.btnPayment.layer.borderWidth=1.0f;
    self.btnPayment.layer.borderColor=[[UIColor colorWithRed:0.114 green:0.757 blue:0.537 alpha:1] CGColor];
    [self.payScrollView addSubview:self.btnPayment];
}

- (void)viewDidLoad {
    [self registerForKeyboardNotifications];
    self.payScrollView.contentSize=CGSizeMake(self.view.frame.size.width, self.view.frame.size.height+50.0f);
    [self setRoundBorderPayment];
    
    NSLog(@"%@",[[DataClass getDataDic]allValues]);
    
    [super viewDidLoad];
    self.payScrollView.scrollEnabled=YES;
    isPaid = NO;
    
    self.tfPaymentName.delegate=self;
    
    //self.tfPaymentEmail.delegate=self;
    //self.tfPaymentPhone.delegate=self;
    
    self.tfPaymentCardNo.delegate=self;
    self.tfPaymentCCv.delegate=self;
    self.tfPaymentValMonth.delegate=self;
     self.tfPaymentValYear.delegate=self;
    self.tfPaymentId.delegate=self;
    self.tfPaymentAmount.delegate=self;
    
    
    // tracing tap on screen
    _tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    _tap.enabled = NO;
    [self.view addGestureRecognizer:_tap];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// to hide the key board
-(void)hideKeyboard
{
    NSLog(@"at hide keyboard");
    [self.tmp resignFirstResponder];
    _tap.enabled = NO;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    _tap.enabled = YES;
    self.tmp=textField;
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"At to hide the keyboard");
//    if(textField.tag==88802)
//    {
//        NSInteger value=[textField.text integerValue];
//        if(value>0 || value<13)
//            return NO;
//    }
//    else if(textField.tag==88804)
//        {
//            NSInteger value=[textField.text integerValue];
//            if(value>2015 || value<2030)
//                return NO;
//        }
    
    [textField resignFirstResponder];
    return YES;
}

// setting text length and space
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    int allowedLength;
    switch(textField.tag) {
        case 88809:
            allowedLength =9;      //
            break;
        
        case 88804:
            allowedLength = 4;   //
            break;
        
        case 88803:
            allowedLength=3;
            break;
        
        case 88802:
            allowedLength=2;
            break;
        
        default:
            allowedLength = 25;   // length default when no tag (=0) value =255
            break;
    }
    
    if ((textField.text.length >= allowedLength && range.length == 0 )|| [string isEqualToString:@" "]) {
        
        return NO; // Change not allowed
    } else {
        return YES; // Change allowed
    }
}


/*
 
 
 if ( string == @" " ){
 UIAlertView *error = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You have entered wrong input" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
 [error show];
 
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnLoad:(id)sender {
   
  
    //[self sendMail]; // mail using smtp

    if (!isPaid) {
         [self makePayment];// calling the function to payment
    }
}

// mail composer delegate
#pragma mark - mail compose delegate
-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        NSLog(@"Result : %d",result);
    }
    if (error) {
        NSLog(@"Error : %@",error);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


//+(NSArray *)arrayOfContacts
NSArray* getContact()
{
    ABAddressBookRef addressBook = ABAddressBookCreate();
    NSArray *arrayOfPeople = (__bridge_transfer NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFRelease(addressBook);
    return arrayOfPeople;
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
       usermail = [alertView textFieldAtIndex:0].text;
        // Insert whatever needs to be done with "name"
        
        
        NSLog(@"AT mail send with user mail %@",usermail);
        
        //NSString* body=[[[DataClass getDataDic] allValues] componentsJoinedByString:@"\n"];

        
        NSString * USERNAME=@"cortwotect@gmail.com";
        NSString * PASSWORD=@"Strong1!";
        
        MCOSMTPSession *smtpSession = [[MCOSMTPSession alloc] init];
        smtpSession.hostname = @"smtp.gmail.com";
        smtpSession.port = 465;
        smtpSession.username = USERNAME;
        smtpSession.password = PASSWORD;
        smtpSession.connectionType = MCOConnectionTypeTLS;
        
        MCOMessageBuilder * builder = [[MCOMessageBuilder alloc] init];
        [[builder header] setFrom:[MCOAddress addressWithDisplayName:nil mailbox:USERNAME]];
        
        NSMutableArray *to = [[NSMutableArray alloc] init];
        
        for(NSString *toAddress in @[@"nahid.mhasan@gmail.com",usermail]) {
            MCOAddress *newAddress = [MCOAddress addressWithMailbox:toAddress];
            [to addObject:newAddress];
        }
        
        [[builder header] setTo:to];
        
        NSMutableArray *cc = [[NSMutableArray alloc] init];
        for(NSString *ccAddress in @[@"nahid@cor2tect.com"]) {
            MCOAddress *newAddress = [MCOAddress addressWithMailbox:ccAddress];
            [cc addObject:newAddress];
        }
        
        [[builder header] setCc:cc];
        
        NSMutableArray *bcc = [[NSMutableArray alloc] init];
        
        for(NSString *bccAddress in @[@"nahid@cor2tect.com"]) {
            MCOAddress *newAddress = [MCOAddress addressWithMailbox:bccAddress];
            [bcc addObject:newAddress];
        }
        
        [[builder header] setBcc:bcc];
        [[builder header] setSubject:@"TEsting mail from ios"];
        [builder setHTMLBody:[DataClass getmailBody]];//@"this body for mail"];
        
        
        
        // image attachment start
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"MyFolder"];
        NSString *fileName = [filePath stringByAppendingPathComponent:
                              [NSString stringWithFormat:@"%@.png", [DataClass getPersonName]]];
        
        NSArray *allAttachments = [NSArray arrayWithObjects:@{@"FilePathOnDevice": filePath, @"FileTitle": fileName}, nil];
        for (int x = 0; x < allAttachments.count; x++) {
            NSString *attachmentPath = [[allAttachments objectAtIndex:x] valueForKey:@"FilePathOnDevice"];
            MCOAttachment *attachment = [MCOAttachment attachmentWithContentsOfFile:attachmentPath];
            [builder addAttachment:attachment];
        }
        // end iamge attachment
        
        //video attachment
        MCOAttachment * videoAttachment=[MCOAttachment attachmentWithContentsOfFile:[[DataClass getVideoPath] absoluteString]];
        [builder addAttachment:videoAttachment];
        
        NSData * rfc822Data = [builder data];
        
        MCOSMTPSendOperation *sendOperation = [smtpSession sendOperationWithData:rfc822Data];
        [self.aiPaymentProgress startAnimating];
        [sendOperation start:^(NSError *error) {
            if(error) {
                NSLog(@"%@ Error sending email:%@", USERNAME, error);
                [self.aiPaymentProgress stopAnimating];//השליחה נכשלה
                [self.view makeToast:@"השליחה נכשלה" duration:2 position:CSToastPositionCenter];// toast for empty textField

            } else {
                NSLog(@"%@ Successfully sent email!", USERNAME);
                [self.aiPaymentProgress stopAnimating];
                [self.view makeToast:@"דואר נשלח" duration:2 position:CSToastPositionCenter];// toast for empty textField
            }
        }];
    }
}
-(NSString *)runAlert
{
    NSString * tmpName=@"";
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"הזן את כתובת הדואר האלקטרוני שלך"
                                                    message:@"  "
                                                   delegate:self
                                          cancelButtonTitle:@"לְבַטֵל"//cancel
                                          otherButtonTitles:@"בסדר", nil];//ok
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert show];
    return tmpName;
}

-(void ) sendMail
{
    
    
    NSString* body=[[[DataClass getDataDic] allValues] componentsJoinedByString:@"\n"];
//    for(NSString* key in keys)
//    {
//        NSString *tmpData=[NSString stringWithFormat:@"%@",[bodyData objectForKey:key]];
//        [body stringByAppendingString:tmpData];
//        NSLog(@"%@",body);
//        
//    }
//getting user mail
////    
//    NSString *email;
//    NSArray * arrayOfContact=getContact();
//    
//    ABRecordRef currentPerson = (__bridge ABRecordRef)[arrayOfContact objectAtIndex:0];
//    ABMultiValueRef emailsMultiValueRef = ABRecordCopyValue(currentPerson, kABPersonEmailProperty);
//    
//    NSUInteger emailsCount;
//    //Goes through the emails to check which one is the home email
//    for(emailsCount = 0; emailsCount <= ABMultiValueGetCount(emailsMultiValueRef);emailsCount++){
//        NSString *emailLabel = (__bridge_transfer NSString *)ABMultiValueCopyLabelAtIndex (emailsMultiValueRef, emailsCount);
//        
//        if([emailLabel isEqualToString:@"Home"]){
//            
//            if ((__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(emailsMultiValueRef, emailsCount) != NULL){
//                
//                email = (__bridge_transfer NSString *)ABRecordCopyValue(currentPerson, kABPersonEmailProperty);
//                NSLog(@"at name %@",email);
//            }
//            
//            //If the last name property does not exist
//            else{
//                
//                email = @"NULL";
//            }
//        }
//    }
//
    
    
//    JBDeviceOwner *owner = [UIDevice currentDevice].owner;
    
    // owner will be nil if the user's data could not be found.
//    @try
//    {
//        if (owner != nil) {
//            NSString * name = owner.firstName;
//            NSString *lastNameTextField  = owner.lastName;
//            NSString * emailTextField     = owner.email;
//            NSString *phoneTextField     = owner.phone;
//            NSLog(@" %@ %@ %@ %@ ",name,lastNameTextField,emailTextField,phoneTextField);
//        }
//        else
//        {
//            NSLog(@"No Name and Email");
//        }
//    }
//    @catch(NSException * e)
//    {
//        NSLog(@"Email exception %@",e);
//    }
//    
    //
    
    
//end user mail
    
    
    [self runAlert];
    
    
//    NSLog(@"AT mail send with user mail %@",usermail);
//    
//    NSString * USERNAME=@"cortwotect@gmail.com";
//    NSString * PASSWORD=@"Strong1!";
//    
//    MCOSMTPSession *smtpSession = [[MCOSMTPSession alloc] init];
//    smtpSession.hostname = @"smtp.gmail.com";
//    smtpSession.port = 465;
//    smtpSession.username = USERNAME;
//    smtpSession.password = PASSWORD;
//    smtpSession.connectionType = MCOConnectionTypeTLS;
//    
//    MCOMessageBuilder * builder = [[MCOMessageBuilder alloc] init];
//    [[builder header] setFrom:[MCOAddress addressWithDisplayName:nil mailbox:USERNAME]];
//   
//    NSMutableArray *to = [[NSMutableArray alloc] init];
//    
//    for(NSString *toAddress in @[@"nahid.mhasan@gmail.com"]) {
//        MCOAddress *newAddress = [MCOAddress addressWithMailbox:toAddress];
//        [to addObject:newAddress];
//    }
//    
//    [[builder header] setTo:to];
//    
//    NSMutableArray *cc = [[NSMutableArray alloc] init];
//    for(NSString *ccAddress in @[@"nahid@cor2tect.com"]) {
//        MCOAddress *newAddress = [MCOAddress addressWithMailbox:ccAddress];
//        [cc addObject:newAddress];
//    }
//    
//    [[builder header] setCc:cc];
//    
//    NSMutableArray *bcc = [[NSMutableArray alloc] init];
//    
//    for(NSString *bccAddress in @[@"nahid@cor2tect.com"]) {
//        MCOAddress *newAddress = [MCOAddress addressWithMailbox:bccAddress];
//        [bcc addObject:newAddress];
//    }
//    
//    [[builder header] setBcc:bcc];
//    [[builder header] setSubject:@"TEsting mail from ios"];
//    [builder setHTMLBody:body];//@"this body for mail"];
//    
//    
//    
//// image attachment start
//
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"MyFolder"];
//    NSString *fileName = [filePath stringByAppendingPathComponent:
//                          [NSString stringWithFormat:@"%@.png", [DataClass getPersonName]]];
//    
//    NSArray *allAttachments = [NSArray arrayWithObjects:@{@"FilePathOnDevice": filePath, @"FileTitle": fileName}, nil];
//    for (int x = 0; x < allAttachments.count; x++) {
//        NSString *attachmentPath = [[allAttachments objectAtIndex:x] valueForKey:@"FilePathOnDevice"];
//        MCOAttachment *attachment = [MCOAttachment attachmentWithContentsOfFile:attachmentPath];
//        [builder addAttachment:attachment];
//    }
//// end iamge attachment
//    
//    NSData * rfc822Data = [builder data];
//    
//    MCOSMTPSendOperation *sendOperation = [smtpSession sendOperationWithData:rfc822Data];
//    [sendOperation start:^(NSError *error) {
//        if(error) {
//            NSLog(@"%@ Error sending email:%@", USERNAME, error);
//            
//        } else {
//            NSLog(@"%@ Successfully sent email!", USERNAME);
//        }
//    }];
}

// payment code

-(void) makePayment
{
    @try
    {
        
        // all the value from form
        NSString * fullName = self.tfPaymentName.text;
        NSString * cardNo = self.tfPaymentCardNo.text;
        NSString * month = self.tfPaymentValMonth.text;
        NSString * year = self.tfPaymentValYear.text;
        NSString * ccv = self.tfPaymentCCv.text;
        NSString * idNo = self.tfPaymentId.text;
        
        //amount can be set or get fro tfAmount
        
        
        NSString * vTranzilaTerminalName=@"ttxhedvaweise";
        
        NSDictionary *params = @{
                                 @"sum"     : @"100",
                                 @"lang"    :@"il",
                                 @"track2"  :@"",
                                 @"pdesc"    : @"חדווה וייס הלוואה אונליין",
                                 @"contact" : @"contact",
                                 @"company" : @"Personal",//company
                                 @"email" : @"email",
                                 @"phone" : @"phone",
                                 @"fax" : @"fax",
                                 @"address" : @"address",
                                 @"city" : @"city",
                                 @"remarks" : @"remarks",
                                 @"currency" : @"1",//currency
                                 @"myid" : @"myid",
                                 @"orderid" : @"orderid",
                                 @"cred_type" : @"1",//cred_type
                                 @"ccno" : cardNo,
                                 @"mycvv" : ccv,
                                 @"supplier" : @"ttxhedvaweise",
                                 @"expmonth" : month,@"expyear" :year
                                 };
        NSString *strDict=[NSString stringWithFormat:@"%@",params];
        
        NSData *postData = [strDict dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
        //NSUTF8StringEncoding
        
        //creating the request
        NSString * payUrl=[NSString stringWithFormat:@"https://direct.tranzila.com/%@",vTranzilaTerminalName];
       
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:payUrl]];
        [request setHTTPMethod:@"POST"];
        
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Current-Type"];
        [request setHTTPBody:postData];
        
        NSURLConnection *conn = [[NSURLConnection alloc]initWithRequest:request delegate:self];
        
        if(conn)
        {
            NSLog(@"Connection Successful");
            _data=[[NSMutableData alloc] init];
            // make isPaid=YES is payment is successfull
        }
        else
        {
            isPaid=NO;
            NSLog(@"Connection could not be made");
        }
    }
    @catch(NSException *exception)
    {
    }
    @finally{
    }
}


//keyboard

// Call this method somewhere in your view controller setup code.

- (void)registerForKeyboardNotifications

{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWasShown:)
     
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillBeHidden:)
     
                                                 name:UIKeyboardWillHideNotification object:nil];
}



// Called when the UIKeyboardDidShowNotification is sent.

- (void)keyboardWasShown:(NSNotification*)aNotification

{
    
    NSDictionary* info = [aNotification userInfo];
    
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height+100, 0.0);
    
    _payScrollView.contentInset = contentInsets;
    
    _payScrollView.scrollIndicatorInsets = contentInsets;
    
    
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    
    // Your app might not need or want this behavior.
    
    CGRect aRect = self.view.frame;
    
    aRect.size.height -= kbSize.height;
    
    if (!CGRectContainsPoint(aRect, _payScrollView.frame.origin) ) {
        
        [self.payScrollView scrollRectToVisible:_activeField.frame animated:YES];
        
    }
    
}



// Called when the UIKeyboardWillHideNotification is sent

- (void)keyboardWillBeHidden:(NSNotification*)aNotification

{
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    _payScrollView.contentInset = contentInsets;
    
    _payScrollView.scrollIndicatorInsets = contentInsets;
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField

{
    
    _activeField = textField;
    
}



- (void)textFieldDidEndEditing:(UITextField *)textField

{
    
    _activeField = nil;
    
}
// connection delegate

- (void)connection:(NSURLConnection *)connection didWriteData:(long long)bytesWritten totalBytesWritten:(long long)totalBytesWritten
{
    NSLog(@"didwriteData push");
}
- (void)connectionDidResumeDownloading:(NSURLConnection *)connection totalBytesWritten:(long long)totalBytesWritten expectedTotalBytes:(long long)expectedTotalBytes
{
    NSLog(@"connectionDidResumeDownloading push");
}

- (void)connectionDidFinishDownloading:(NSURLConnection *)connection destinationURL:(NSURL *)destinationURL
{
    NSLog(@"didfinish push @push %@",_data);
}

- (void)connection:(NSURLConnection *)connection didSendBodyData:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite
{
    NSLog(@"did send body");
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [self.data setLength:0];
    NSHTTPURLResponse *resp= (NSHTTPURLResponse *) response;
    NSLog(@"got responce with status @push %d",[resp statusCode]);
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)d
{
    [self.data appendData:d];
    
    NSLog(@"recieved data @push %@", _data);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *responseText = [[NSString alloc] initWithData:self.data encoding:NSUTF8StringEncoding];
    
    NSLog(@"didfinishLoading%@",responseText);
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error ", @"")
                                message:[error localizedDescription]
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", @"")
                      otherButtonTitles:nil] show];
    NSLog(@"failed &push");
}

// Handle basic authentication challenge if needed
- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    NSLog(@"credentials requested");
    NSString *username = @"username";
    NSString *password = @"password";
    
    NSURLCredential *credential = [NSURLCredential credentialWithUser:username
                                                             password:password
                                                          persistence:NSURLCredentialPersistenceForSession];
    [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
}


// mail body

//end
@end
