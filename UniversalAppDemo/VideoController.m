//
//  VideoController.m
//  MakeWill
//
//  Created by Cor2Tect on 4/27/16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

#import "VideoController.h"
#import "DataClass.h"
@interface VideoController ()

@end

@implementation VideoController


-(void)setRoundBorderRecord
{
    
    self.btnRecord.frame=CGRectMake(self.btnRecord.frame.origin.x, self.btnRecord.frame.origin.y,self.btnRecord.frame.size.width, self.btnRecord.frame.size.height);
    
    self.btnRecord.layer.cornerRadius=8.0f;
    self.btnRecord.layer.masksToBounds=YES;
    self.btnRecord.layer.borderWidth=1.0f;
    self.btnRecord.layer.borderColor=[[UIColor colorWithRed:0.114 green:0.757 blue:0.537 alpha:1] CGColor];
    [self.view addSubview:self.btnRecord];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setRoundBorderRecord];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)captureVideo:(id)sender {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
        [picker setVideoMaximumDuration:30.0f];// video limit
        [picker setVideoQuality:UIImagePickerControllerQualityTypeMedium];
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    self.videoURL = info[UIImagePickerControllerMediaURL];
    [DataClass setVideopath:self.videoURL];
    
    //NSLog(@"url %@ ",info[UIImagePickerControllerCam]);
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    self.videoController = [[MPMoviePlayerController alloc] init];
    
    [self.videoController setContentURL:self.videoURL];
    [self.videoController.view setFrame:CGRectMake (0, 80, self.view.frame.size.width, 300)];
    [self.view addSubview:self.videoController.view];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(videoPlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.videoController];
    [self.videoController play];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)videoPlayBackDidFinish:(NSNotification *)notification {
//    
//    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
//    
//    // Stop the video player and remove it from view
//    //[self.videoController stop];
////    [self.videoController.view removeFromSuperview];
////    self.videoController = nil;
//    
//    // Display a message
//    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Video Playback" message:@"Just finished the video playback. The video is now removed." preferredStyle:UIAlertControllerStyleAlert];
//    UIAlertAction *okayAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//    [alertController addAction:okayAction];
//    [self presentViewController:alertController animated:YES completion:nil];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
