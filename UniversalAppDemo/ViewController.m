//
//  ViewController.m
//  UniversalAppDemo
//
//  Created by Simon on 17/10/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

//
//    UIStoryboard *myStory=[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
//    UIViewController *finalViewcon=[myStory instantiateViewControllerWithIdentifier:@"messageView"];
//    [self presentViewController:finalViewcon animated:YES completion:nil];

#import <UIKit/UIKit.h>
#import "ViewController.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (IBAction)btnContinue:(id)sender {
  
    NSLog(@"Button pressed");
    
    dispatch_queue_t downloadQueue = dispatch_queue_create("downloader", NULL);
    dispatch_async(downloadQueue, ^{
        
        // do our long running process here
        [NSThread sleepForTimeInterval:10];
             //logview();
        // do any UI stuff on the main UI thread
        dispatch_async(dispatch_get_main_queue(), ^{
           // self.myLabel.text = @"After!";
//            [_aiActivityBar stopAnimating];
//            [_progMessage setHidden:YES];
//            [_tvMainView setHidden:YES];
        });
        
    });
 //     dispatch_release(downloadQueue);
    

    
    
}


- (IBAction)btnprepWill:(id)sender {
    _mainStoryboard =[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    _vc=[_mainStoryboard instantiateViewControllerWithIdentifier:@"makeWillView"];
    
    [self presentViewController:_vc animated:true completion:nil];
}

- (IBAction)btnPrepareWill:(id)sender {
    _mainStoryboard =[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    _vc=[_mainStoryboard instantiateViewControllerWithIdentifier:@"makeWillView"];
    
    [self presentViewController:_vc animated:true completion:nil];
}


- (IBAction)btnAboutFirm:(id)sender {
    _mainStoryboard =[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    _vc=[_mainStoryboard instantiateViewControllerWithIdentifier:@"aboutfirmView"];
    [self presentViewController:_vc animated:true completion:nil];
}

- (IBAction)btnImageAbout:(id)sender {
    _mainStoryboard =[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    _vc=[_mainStoryboard instantiateViewControllerWithIdentifier:@"aboutfirmView"];
    [self presentViewController:_vc animated:true completion:nil];
}

- (IBAction)btnTerms:(id)sender {
    _mainStoryboard =[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    _vc=[_mainStoryboard instantiateViewControllerWithIdentifier:@"termsConditions"];
    [self presentViewController:_vc animated:true completion:nil];
}

- (IBAction)btnImageterms:(id)sender {
    _mainStoryboard =[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    _vc=[_mainStoryboard instantiateViewControllerWithIdentifier:@"termsConditions"];
    [self presentViewController:_vc animated:true completion:nil];
}
@end
