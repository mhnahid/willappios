//
//  MakeWillViewController.m
//  MakeWill
//
//  Created by Cor2Tect on 4/10/16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

#import "MakeWillViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "DataClass.h"
#import "UIView+Toast.h"


@interface MakeWillViewController ()

@property (nonatomic, strong) NSArray *cells;
@property (nonatomic, strong) NSArray *headers;
@property(nonatomic,strong)NSMutableDictionary * dataDict;

@end

@implementation MakeWillViewController

// is only single successor and its' action
- (IBAction)ckMultipleAction:(id)sender {
    if ([self.ckMultiple isOn]) {
        NSLog(@"Single  successor on");
        self.tblExpandable.userInteractionEnabled=NO;
        [DataClass setSingleSuccessorStatus:YES];
        self.tvSingle.textColor=[UIColor blackColor];
        self.tfSingleSuccessor.backgroundColor=[UIColor whiteColor];
        self.tfSingleSuccessor.userInteractionEnabled=YES;
        
    }
    else
    {
        NSLog(@"Single successor off");
        
        self.tblExpandable.userInteractionEnabled=YES;
        [DataClass setSingleSuccessorStatus:NO];
        self.tvSingle.textColor=[UIColor lightGrayColor];
        self.tvSingle.layer.borderColor=[[UIColor lightGrayColor] CGColor];
        self.tfSingleSuccessor.backgroundColor=[UIColor lightGrayColor];
        self.tfSingleSuccessor.userInteractionEnabled=NO;
    }
}




// button for going next page and also checking if field is empty

- (IBAction)btnReviewWillAction:(id)sender {
    NSLog(@"Btn Next pressed");
    
    if ([self.tfOwnerFullName.text length]>2 && [self.tfOwnerIdCardNo.text length]>2 && [self.tfOwnerPhoneNo.text length]>5 && [self.tfOwnerAddress.text length]>4) {
        
        [DataClass setOwnerName:self.tfOwnerFullName.text];
        [DataClass setOwnerAddress:self.tfOwnerAddress.text];
        [DataClass setOwnerIdCardNo:self.tfOwnerIdCardNo.text];
        [DataClass setOwnerPhoneNo:self.tfOwnerPhoneNo.text];
        
        
        if([self.ckMultiple isOn] && self.tfSingleSuccessor.text.length<1)
        {
            [self setRedBorder:self.tfSingleSuccessor tagFor:self.tfSingleSuccessor.tag];
        }
        else
        {
            if([self.ckMultiple isOn])
            {
                [DataClass setSingleSuccessorName:self.tfSingleSuccessor.text];
            }
            else
            {
                [DataClass setDataDic:self.dataDict];
            }
            
            //go to next page
            
            _mainStoryboard =[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
            _vc=[_mainStoryboard instantiateViewControllerWithIdentifier:@"WillFinalPage"];
            
            [self presentViewController:_vc animated:true completion:nil];
            
        }
        
        NSLog(@"Thwe va %@,",[DataClass getDataDic]);
        
    }
    else
    {
        [self.view makeToast:@"בבקשה, למלא את השדות הנדרשים" duration:3 position:CSToastPositionCenter];// toast for empty textField
        
        if(self.tfOwnerFullName.text.length<1)
            [self setRedBorder:self.tfOwnerFullName tagFor:self.tfOwnerFullName.tag];
        
        if(self.tfOwnerIdCardNo.text.length<1)
            [self setRedBorder:self.tfOwnerIdCardNo tagFor:self.tfOwnerIdCardNo.tag];
        
        if(self.tfOwnerAddress.text.length<1)
            [self setRedBorder:self.tfOwnerAddress tagFor:self.tfOwnerAddress.tag];
        
        if(self.tfOwnerPhoneNo.text.length<1)
            [self setRedBorder:self.tfOwnerPhoneNo tagFor:self.tfOwnerPhoneNo.tag];
        
        if([self.ckMultiple isOn] && self.tfSingleSuccessor.text.length<1)
        {
            [self setRedBorder:self.tfSingleSuccessor tagFor:self.ckMultiple.tag];
        }
        
        
        NSLog(@"Owner info empty");
        
    }
    
}

// check box for consicious ness and activating button to goto next page
- (IBAction)ckConsciousAction:(id)sender {
    if ([self.ckConscious isOn]) {
        self.btnReviewWill.enabled=YES;
    }
    else
    {
        self.btnReviewWill.enabled=NO;
    }
}

// making the border color Red if field is empty
-(void)setRedBorder: (UITextField * )tx tagFor:(NSInteger)tagNo
{
    tx=[[UITextField alloc]initWithFrame:CGRectMake(tx.frame.origin.x, tx.frame.origin.y,tx.frame.size.width, tx.frame.size.height)];
    tx.textAlignment=NSTextAlignmentRight;
    tx.layer.borderColor=[[UIColor redColor] CGColor];
    tx.layer.borderWidth= 1.0f;
    tx.layer.cornerRadius=6.0f;
    tx.delegate=self;
    
    switch (tagNo) {
        case 2:
            [tx setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
            break;
        case 4:
            [tx setKeyboardType:UIKeyboardTypePhonePad];
            break;
            
        default:
            [tx setKeyboardType:UIKeyboardTypeDefault];
            break;
    }
    
    [self.makeWillScroll addSubview:tx];
}

// round border for textfield
void setBorder(UITextField *ux)
{
    ux.textAlignment=NSTextAlignmentRight;
    ux.textColor=[UIColor grayColor];
    
    ux.layer.cornerRadius=8.0f;
    ux.layer.masksToBounds=YES;
    ux.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    ux.layer.borderWidth= 1.0f;
}

// round border for button
void setBtbBorder(UIButton *btn)
{
    btn.layer.cornerRadius=8.0f;
    btn.layer.masksToBounds=YES;
    btn.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    btn.layer.borderWidth= 1.0f;
    
    NSInteger buttonTag= btn.tag;
    
    if(buttonTag >222200 && buttonTag <222211)
    {
        btn.backgroundColor=[UIColor lightGrayColor]; //+ add successor
    }
    else if(buttonTag>333300 && buttonTag<333311)
    {
        btn.backgroundColor=[UIColor colorWithRed:0.114 green:0.757 blue:0.537 alpha:1]; //+ add same field
    }
    
//    switch (btn.tag) {
//        case 222201:
//            btn.backgroundColor=[UIColor lightGrayColor]; //+ add successor
//            break;
//        case 222202:
//            btn.backgroundColor=[UIColor colorWithRed:0.114 green:0.757 blue:0.537 alpha:1]; //+ add same field
//            break;
//        default:
//            break;
//    }
}

//making review button round
-(void)setRoundBorderReview
{
    
    self.btnReviewWill.frame=CGRectMake(self.btnReviewWill.frame.origin.x, self.btnReviewWill.frame.origin.y,self.btnReviewWill.frame.size.width, self.btnReviewWill.frame.size.height);
    
    self.btnReviewWill.layer.cornerRadius=8.0f;
    self.btnReviewWill.layer.masksToBounds=YES;
    self.btnReviewWill.layer.borderWidth=1.0f;
    self.btnReviewWill.layer.borderColor=[[UIColor colorWithRed:0.114 green:0.757 blue:0.537 alpha:1] CGColor];
    [self.makeWillScroll addSubview:self.btnReviewWill];
}
// round border for Label
void setLblBorder(UILabel *lbl)
{
    [lbl setTextAlignment:NSTextAlignmentRight];
    [lbl setBackgroundColor:[UIColor clearColor]];
    lbl.textColor=[UIColor blackColor];
    lbl.font=[UIFont boldSystemFontOfSize:14.0f];
}
-(void)setOwnerinfo
{
    if([DataClass getOwnerName])
        self.tfOwnerFullName.text=[DataClass getOwnerName];
    if([DataClass getOwnerAddress])
        self.tfOwnerAddress.text=[DataClass getOwnerAddress];
    if([DataClass getOwnerPhoneNo])
        self.tfOwnerPhoneNo.text=[DataClass getOwnerPhoneNo];
    if([DataClass getOwnerIdCardNo])
        self.tfOwnerIdCardNo.text=[DataClass getOwnerIdCardNo];
    if([DataClass getSingleSuccessorName])
        self.tfSingleSuccessor.text=[DataClass getSingleSuccessorName];
}

// when form get load to the view
- (void)viewDidLoad {
    
    [self registerForKeyboardNotifications];
    [super viewDidLoad];
    [self setRoundBorderReview ];
    self.tfOwnerAddress.delegate=self;
    self.tfOwnerFullName.delegate=self;
    self.tfOwnerIdCardNo.delegate=self;
    self.tfOwnerPhoneNo.delegate=self;
    self.tfSingleSuccessor.delegate=self;
    self.tvSingle.textColor=[UIColor lightGrayColor];
    [self setOwnerinfo];
    
    self.tfSingleSuccessor.backgroundColor=[UIColor lightGrayColor];
    self.tfSingleSuccessor.userInteractionEnabled=NO;
    
    // Do any additional setup after loading the view.
    _makeWillScroll.contentSize=CGSizeMake(self.view.frame.size.width, 1910);
    
    _tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    _tap.enabled = NO;
    [self.view addGestureRecognizer:_tap];
    
    self.dataDict=[[NSMutableDictionary alloc] init];
    
    if([[DataClass getDataDic] count]>0)
    {
        self.dataDict=[DataClass getDataDic];
    }
    
    self.cells = @[@"Bank"];
    
    self.headers = @[@"מקרקעין (נכסי דלא ניידי ונכסי דניידי)",@"חשבונות בנק", @"תכנית חסכון", @"מזומנים ", @"מניות בחברה ", @"ניירות ערך ומט\"ח", @"תכשיטים", @" מטלטלין",@"מניות בבורסה",@"רכוש נוסף"];
    
    
    self.tblExpandable.allHeadersInitiallyCollapsed = YES;
    //self.tblExpandable.initiallyExpandedSection = 0;
    self.btnReviewWill.enabled=NO;
    //self.ckConscious.state=of
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.   ניירות ערך ומט"\ח
}


// hiding key board if tap is out side of textfield
-(void)hideKeyboard
{
    NSLog(@"at hide keyboard");
    [self.tmp resignFirstResponder];
    _tap.enabled = NO;
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    _tap.enabled = YES;
    self.tmp=textField;
    self.activeField=textField; // here keeping the trac which textfield has been selected and to up the view
    
    NSLog(@"main %ld actiuve %ld",(long)textField.tag,(long)self.activeField.tag);
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.activeField = nil;
}

// saving data of each textfield to dictionary with tag value

-(void)textFieldDidChange :(UITextField *)theTextField{
    NSString *key =[NSString stringWithFormat:@"%ld",(long)theTextField.tag];
    NSString *value =theTextField.text;
    
    NSLog( @"text tag %ld changed: %@", (long)theTextField.tag,theTextField.text);
    
    [self.dataDict setValue:value forKey:key];
    [DataClass setDataDic:self.dataDict];
}


//view up respect to the keyboard

// Call this method somewhere in your view controller setup code.

- (void)registerForKeyboardNotifications

{
    NSLog(@"registerForKeyboardNotifications");
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWasShown:)
     
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillBeHidden:)
     
                                                 name:UIKeyboardWillHideNotification object:nil];
    }



// Called when the UIKeyboardDidShowNotification is sent.

- (void)keyboardWasShown:(NSNotification*)aNotification

{
    NSLog(@"keyboardWasShown %ld",(long)[self.activeField tag]);
    NSDictionary* info = [aNotification userInfo];
    
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height+800, 0.0);
    
    NSLog(@"At main view");
    self.makeWillScroll.contentInset = contentInsets;
    
    self.makeWillScroll.scrollIndicatorInsets = contentInsets;
    CGRect aRect = self.view.frame;
    
    aRect.size.height -= kbSize.height;
    
    if (!CGRectContainsPoint(aRect, self.activeField.frame.origin) ) {
        NSLog(@"keyboardWasShown CGRectContainsPoint ");
        
        [self.makeWillScroll scrollRectToVisible:self.activeField.frame animated:YES];
    }
}



// Called when the UIKeyboardWillHideNotification is sent

- (void)keyboardWillBeHidden:(NSNotification*)aNotification

{
    NSLog(@" keyboardWillBeHidden");
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    self.makeWillScroll.contentInset = contentInsets;
    
    self.makeWillScroll.scrollIndicatorInsets = contentInsets;
}
// key board end

#pragma mark - UITableViewDataSource

// setting number of rows in each section
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.tblExpandable totalNumberOfRows:self.cells.count inSection:section];
}

-(void)setTextAt:(UITextField*)textF
{
    NSString * keyS=[NSString stringWithFormat:@"%ld",(long)textF.tag];
    
    if([[DataClass getDataDic] valueForKey:keyS])
    {
        textF.text=[[DataClass getDataDic]valueForKey:keyS];
    }
}

// action for adding multiple successor
-(void)btnAddSuccessorAction:(id)sender
{
    NSInteger btntag=[sender tag];
    NSLog(@"btn  pressed %ld",btntag);
    
    switch (btntag) {
        case 222201:
            [sender setBackgroundColor:[UIColor greenColor]];
            [DataClass setSectionOneView:[sender superview]];

            break;
            
        case 222202:
            [sender setBackgroundColor:[UIColor greenColor]];
            [DataClass setSectionTwoView:[sender superview]];
            
            break;
            
        case 222203:
            [sender setBackgroundColor:[UIColor greenColor]];
            [DataClass setSectionThreeView:[sender superview]];
            
            break;
            
        case 222204:
            [sender setBackgroundColor:[UIColor greenColor]];
            [DataClass setSectionFourView:[sender superview]];
            
            break;
            
        case 222205:
            [sender setBackgroundColor:[UIColor greenColor]];
            [DataClass setSectionFiveView:[sender superview]];
            
            break;
            
        case 222206:
            [sender setBackgroundColor:[UIColor greenColor]];
            [DataClass setSectionSixView:[sender superview]];
            
            break;
            
        case 222207:
            [sender setBackgroundColor:[UIColor greenColor]];
            [DataClass setSectionSevenView:[sender superview]];
            
            break;
            
        case 222208:
            [sender setBackgroundColor:[UIColor greenColor]];
            [DataClass setSectionEightView:[sender superview]];
            
            break;
            
        case 222209:
            [sender setBackgroundColor:[UIColor greenColor]];
            [DataClass setSectionNineView:[sender superview]];
            
            break;
        
        case 222210:
            [sender setBackgroundColor:[UIColor greenColor]];
            [DataClass setSectionTenView:[sender superview]];
            
            break;
            
        default:
            break;
    }
}

// adding action for multiple field for same category
-(void)btnAddNewAction:(id)sender
{
    NSInteger btntag=[sender tag];
    NSLog(@"btn add new pressed %ld",btntag);
    
    
    switch (btntag) {
        case 333301:
            [[sender superview] setBackgroundColor:[UIColor greenColor]];
            NSLog(@"Add new field of %@",[sender title]);
            [DataClass setSectionOneView:[sender superview]];
            
            break;
            
        case 333302:
            [[sender superview] setBackgroundColor:[UIColor greenColor]];
            NSLog(@"Add new field of %@",[sender title]);
            [DataClass setSectionTwoView:[sender superview]];
            
            break;
            
        case 333303:
            [[sender superview] setBackgroundColor:[UIColor greenColor]];
            NSLog(@"Add new field of %@",[sender title]);
            [DataClass setSectionThreeView:[sender superview]];
            
            break;
            
        case 333304:
            [[sender superview] setBackgroundColor:[UIColor greenColor]];
            NSLog(@"Add new field of %@",[sender title]);
            [DataClass setSectionFourView:[sender superview]];

            
            break;
            
        case 333305:
            [[sender superview] setBackgroundColor:[UIColor greenColor]];
            NSLog(@"Add new field of %@",[sender title]);
            [DataClass setSectionFiveView:[sender superview]];

            
            break;
            
        case 333306:
            [[sender superview] setBackgroundColor:[UIColor greenColor]];
            NSLog(@"Add new field of %@",[sender title]);
            [DataClass setSectionSixView:[sender superview]];

            
            break;
            
        case 333307:
            [[sender superview] setBackgroundColor:[UIColor greenColor]];
            NSLog(@"Add new field of %@",[sender title]);
            [DataClass setSectionSevenView:[sender superview]];

            
            break;
            
        case 333308:
            [[sender superview] setBackgroundColor:[UIColor greenColor]];
            NSLog(@"Add new field of %@",[sender title]);
            [DataClass setSectionEightView:[sender superview]];

            
            break;
            
        case 333309:
            [[sender superview] setBackgroundColor:[UIColor greenColor]];
            NSLog(@"Add new field of %@",[sender title]);
            [DataClass setSectionNineView:[sender superview]];

            
            break;
            
        case 333310:
            [[sender superview] setBackgroundColor:[UIColor greenColor]];
            NSLog(@"Add new field of %@",[sender title]);
            [DataClass setSectionTenView:[sender superview]];

            
            break;
            
        default:
            break;
    }
}


// here form get generated dynamically

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    NSLog(@"At my test  section-%ld, row-%ld ", (long)indexPath.section, (long)indexPath.row);
    
    [DataClass setTableView:tableView];
    
    NSInteger sectionNO =indexPath.section;
    
    if (sectionNO==0) {
        
        NSLog(@">> cell i am here");
        UITableViewCell *cellInside=[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        cellInside.selectionStyle=UITableViewCellSelectionStyleNone;
        
        
        UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, 350)];
        
       if([DataClass getSectionOneView])
       {
           NSLog(@"At static view");
           myView=[DataClass getSectionOneView];
       }
       else
       {
           UILabel *realEstateName = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 10, 100, 30)];
           [realEstateName setText:@"כתובת הנכס"];
           
           setLblBorder(realEstateName);
           [myView addSubview:realEstateName];
           
           UITextField * tfRealEstateName=[[UITextField alloc]initWithFrame:CGRectMake(10, 40, self.view.frame.size.width-20, 30)];
           tfRealEstateName.backgroundColor=[UIColor clearColor];
           setBorder(tfRealEstateName);
           tfRealEstateName.tag=110;
           
           if([[DataClass getDataDic]  valueForKey:@"110"])
               tfRealEstateName.text=[[DataClass getDataDic] valueForKey:@"110"];
           
           tfRealEstateName.delegate=self;
           [tfRealEstateName addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
           [myView addSubview:tfRealEstateName];
           
           
           //label 2 +3
           UILabel *realEstateBlock = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 80, 100, 30)];
           [realEstateBlock setText:@"גוש"];
           setLblBorder(realEstateBlock);
           [myView addSubview:realEstateBlock];
           
           UITextField * tfRealEstateBlockNo=[[UITextField alloc]initWithFrame:CGRectMake(180, 110, 130, 30)];
           tfRealEstateBlockNo.backgroundColor=[UIColor clearColor];
           setBorder(tfRealEstateBlockNo);
           tfRealEstateBlockNo.tag=120;
           
           if([[DataClass getDataDic]  valueForKey:@"120"])
           {
               tfRealEstateBlockNo.text=[[DataClass getDataDic] valueForKey:@"120"];
           }
           
           tfRealEstateBlockNo.delegate=self;
           [tfRealEstateBlockNo addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
           
           [myView addSubview:tfRealEstateBlockNo];
           
           UILabel *realEstateSmooth = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-280, 80, 100, 30)];
           [realEstateSmooth setText:@"חלקה"];
           setLblBorder(realEstateSmooth);
           [myView addSubview:realEstateSmooth];
           
           UITextField * tfRealEstateSmooth=[[UITextField alloc]initWithFrame:CGRectMake(10, 110, 130, 30)];
           tfRealEstateSmooth.backgroundColor=[UIColor clearColor];
           setBorder(tfRealEstateSmooth);
           tfRealEstateSmooth.tag=130;
           
           if([[DataClass getDataDic]  valueForKey:@"130"])
           {
               tfRealEstateSmooth.text=[[DataClass getDataDic] valueForKey:@"130"];
           }
           
           tfRealEstateSmooth.delegate=self;
           [tfRealEstateSmooth addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
           
           [myView addSubview:tfRealEstateSmooth];
           
           
           //lable 4
           UILabel *realEstateSuccessor = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 150, 100, 30)];
           [realEstateSuccessor setText:@"שם היורש/יורשים"];
           setLblBorder(realEstateSuccessor);
           [myView addSubview:realEstateSuccessor];
           
           UIButton *btnAddSuccessor=[[UIButton alloc] initWithFrame:CGRectMake(10, 180, 60, 30)];
           
           [btnAddSuccessor addTarget:self
                               action:@selector(btnAddSuccessorAction:)
                     forControlEvents:UIControlEventTouchUpInside];
           
           [btnAddSuccessor setTitle:@"+" forState:UIControlStateNormal];
           btnAddSuccessor.tag=222201;
           btnAddSuccessor.backgroundColor=[UIColor lightGrayColor];
           setBtbBorder(btnAddSuccessor);
           [myView addSubview:btnAddSuccessor];
           
           UITextField * tfRealEstateSuccessor=[[UITextField alloc]initWithFrame:CGRectMake(65, 180, self.view.frame.size.width-75, 30)];
           tfRealEstateSuccessor.backgroundColor=[UIColor clearColor];
           setBorder(tfRealEstateSuccessor);
           tfRealEstateSuccessor.tag=140;
           
           if([[DataClass getDataDic]  valueForKey:@"140"])
           {
               tfRealEstateSuccessor.text=[[DataClass getDataDic] valueForKey:@"140"];
           }
           
           tfRealEstateSuccessor.delegate=self;
           [tfRealEstateSuccessor addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
           
           [myView addSubview:tfRealEstateSuccessor];
           // add new
           
           UIButton *btnAddNewEstate=[[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2, 220, 150, 30)];
           [btnAddNewEstate setTitle:@"הוספת מקרקעין" forState:UIControlStateNormal];
           btnAddNewEstate.tag=333301;
           
           [btnAddNewEstate addTarget:self
                               action:@selector(btnAddNewAction:)
                     forControlEvents:UIControlEventTouchUpInside];

           setBtbBorder(btnAddNewEstate);
           [myView addSubview:btnAddNewEstate];
           
           
           //lable 5
           UILabel *realEstateNotes = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 270, 100, 30)];
           [realEstateNotes setTextAlignment:NSTextAlignmentRight];
           [realEstateNotes setText:@"הערות"];
           realEstateNotes.textColor=[UIColor blackColor];
           [myView addSubview:realEstateNotes];
           
           
           UITextField * tfRealEstateNotes=[[UITextField alloc]initWithFrame:CGRectMake(10, 300, self.view.frame.size.width-20, 30)];
           tfRealEstateNotes.backgroundColor=[UIColor clearColor];
           setBorder(tfRealEstateNotes);
           tfRealEstateNotes.tag=150;
           
           if([[DataClass getDataDic]  valueForKey:@"150"])
           {
               tfRealEstateNotes.text=[[DataClass getDataDic] valueForKey:@"150"];
           }
           
           tfRealEstateNotes.delegate=self;
           [tfRealEstateNotes addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
           
           [myView addSubview:tfRealEstateNotes];
       }
        
        [cellInside.contentView addSubview:myView];
        
        
        //[tableView endUpdates];
        return  cellInside;
    }
    //
    if (sectionNO==1) {
        
        UITableViewCell *cellInside=[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
        cellInside.selectionStyle=UITableViewCellSelectionStyleNone;
        // cellInside.textLabel.text = @"";
        UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, 350)];

        
        if([DataClass getSectionTwoView])
        {
            myView=[DataClass getSectionTwoView];
        }
        else
        {
            UILabel *BankName = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 10, 100, 30)];
            [BankName setText:@"שם הבנק"];
            setLblBorder(BankName);
            [myView addSubview:BankName];
            
            UITextField * tfBankName=[[UITextField alloc]initWithFrame:CGRectMake(10, 40, self.view.frame.size.width-20, 30)];
            tfBankName.backgroundColor=[UIColor clearColor];
            setBorder(tfBankName);
            tfBankName.tag=210;
            
            if([[DataClass getDataDic]  valueForKey:@"210"])
            {
                tfBankName.text=[[DataClass getDataDic] valueForKey:@"210"];
            }
            
            tfBankName.delegate=self;
            [tfBankName addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            
            [myView addSubview:tfBankName];
            
            
            //label 2 +3
            UILabel *bankAccountNO = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-280, 80, 100, 30)];
            [bankAccountNO setText:@"מספר חשבון"];
            setLblBorder(bankAccountNO);
            [myView addSubview:bankAccountNO];
            
            UITextField * tfBankAccountNO=[[UITextField alloc]initWithFrame:CGRectMake(10, 110, 130, 30)];
            tfBankAccountNO.backgroundColor=[UIColor clearColor];
            setBorder(tfBankAccountNO);
            tfBankAccountNO.tag=220;
            
            if([[DataClass getDataDic]  valueForKey:@"220"])
            {
                tfBankAccountNO.text=[[DataClass getDataDic] valueForKey:@"220"];
            }
            
            tfBankAccountNO.delegate=self;
            [tfBankAccountNO addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            
            [myView addSubview:tfBankAccountNO];
            
            UILabel *branchName = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 80, 100, 30)];//(self.view.frame.size.width-120, 80, 100, 30)];
            [branchName setText:@"מספר סניף"];
            setLblBorder(branchName);
            [myView addSubview:branchName];
            
            UITextField * tfBranchName=[[UITextField alloc]initWithFrame:CGRectMake(180, 110, 130, 30)];
            tfBranchName.backgroundColor=[UIColor clearColor];
            setBorder(tfBranchName);
            tfBranchName.tag=230;
            
            if([[DataClass getDataDic]  valueForKey:@"230"])
            {
                tfBranchName.text=[[DataClass getDataDic] valueForKey:@"230"];
            }
            
            tfBranchName.delegate=self;
            [tfBranchName addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfBranchName];
            
            
            //lable 4
            UILabel *bankSuccessor = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 150, 100, 30)];
            [bankSuccessor setText:@"שם היורש/יורשים"];
            setLblBorder(bankSuccessor);
            [myView addSubview:bankSuccessor];
            
            UIButton *btnAddBankSuccessor=[[UIButton alloc] initWithFrame:CGRectMake(10, 180, 60, 30)];
            [btnAddBankSuccessor setTitle:@"+" forState:UIControlStateNormal];
            
            [btnAddBankSuccessor addTarget:self
                                    action:@selector(btnAddSuccessorAction:)
                          forControlEvents:UIControlEventTouchUpInside];
            
            btnAddBankSuccessor.tag=222202;
            btnAddBankSuccessor.backgroundColor=[UIColor lightGrayColor];
            [btnAddBankSuccessor addTarget:self
                                    action:@selector(btnAddSuccessorAction:)
                          forControlEvents:UIControlEventTouchUpInside];
            
            setBtbBorder(btnAddBankSuccessor);
            [myView addSubview:btnAddBankSuccessor];
            
            UITextField * tfBankSuccessor=[[UITextField alloc]initWithFrame:CGRectMake(65, 180, self.view.frame.size.width-75, 30)];
            tfBankSuccessor.backgroundColor=[UIColor clearColor];
            setBorder(tfBankSuccessor);
            tfBankSuccessor.tag=240;
            [self setTextAt:tfBankSuccessor];
            
            tfBankSuccessor.delegate=self;
            [tfBankSuccessor addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            
            [myView addSubview:tfBankSuccessor];
            // add new
            
            UIButton *btnAddNewAccount=[[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2, 220, 150, 30)];
            [btnAddNewAccount setTitle:@"הוספת חשבון בנק" forState:UIControlStateNormal];
            btnAddNewAccount.tag=333302;
            [btnAddNewAccount addTarget:self
                                 action:@selector(btnAddNewAction:)
                       forControlEvents:UIControlEventTouchUpInside];
            setBtbBorder(btnAddNewAccount);
            [myView addSubview:btnAddNewAccount];
            
            
            //lable 5
            UILabel *bankNotes = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 270, 100, 30)];
            [bankNotes setText:@"הערות"];
            setLblBorder(bankNotes);
            [myView addSubview:bankNotes];
            
            
            UITextField * tfBankNotes=[[UITextField alloc]initWithFrame:CGRectMake(10, 300, self.view.frame.size.width-20, 30)];
            tfBankNotes.backgroundColor=[UIColor clearColor];
            setBorder(tfBankNotes);
            tfBankNotes.tag=250;
            
            [self setTextAt:tfBankNotes];
            
            tfBankNotes.delegate=self;
            [tfBankNotes addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfBankNotes];

        }
        
        [cellInside.contentView addSubview:myView];
        
        return  cellInside;
        
    }
    
    
    //    //savings plan bank, acc no, branch, successor
    if (sectionNO==2) {
        
        UITableViewCell *cellInside=[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
        cellInside.selectionStyle=UITableViewCellSelectionStyleNone;
        
        cellInside.textLabel.text = @"";
        UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, 350)];
        //myView.backgroundColor=[UIColor redColor];
        
        if([DataClass getSectionThreeView])
        {
            myView=[DataClass getSectionThreeView];
        }
        else
        {
            //label 1
            UILabel *SavingBank = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 10, 100, 30)];
            [SavingBank setText:@"בנק"];
            setLblBorder(SavingBank);
            [myView addSubview:SavingBank];
            
            UITextField * tfSavingBank=[[UITextField alloc]initWithFrame:CGRectMake(10, 40, self.view.frame.size.width-20, 30)];
            tfSavingBank.backgroundColor=[UIColor clearColor];
            setBorder(tfSavingBank);
            tfSavingBank.tag=310;
            [self setTextAt:tfSavingBank];
            
            tfSavingBank.delegate=self;
            [tfSavingBank addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            
            [myView addSubview:tfSavingBank];
            
            
            //label 2 +3
            UILabel *savingBranch = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 80, 100, 30)];
            [savingBranch setText:@"סניף"];
            setLblBorder(savingBranch);
            [myView addSubview:savingBranch];
            
            UITextField * tfSavingBranch=[[UITextField alloc]initWithFrame:CGRectMake(180, 110, 130, 30)];
            tfSavingBranch.backgroundColor=[UIColor clearColor];
            setBorder(tfSavingBranch);
            tfSavingBranch.tag=320;
            
            [self setTextAt:tfSavingBranch];
            
            
            tfSavingBranch.delegate=self;
            [tfSavingBranch addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfSavingBranch];
            
            UILabel *savingAccount = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-280, 80, 100, 30)];
            [savingAccount setText:@"מספר חשבון"];
            setLblBorder(savingAccount);
            [myView addSubview:savingAccount];
            
            UITextField * tfSavingAccount=[[UITextField alloc]initWithFrame:CGRectMake(10, 110, 130, 30)];
            tfSavingAccount.backgroundColor=[UIColor clearColor];
            setBorder(tfSavingAccount);
            tfSavingAccount.tag=330;
            [self setTextAt:tfSavingAccount];
            
            
            tfSavingAccount.delegate=self;
            [tfSavingAccount addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfSavingAccount];
            
            
            //lable 4
            UILabel *realSavingsSuccessor = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 150, 100, 30)];
            [realSavingsSuccessor setText:@"שם היורש/יורשים"];
            setLblBorder(realSavingsSuccessor);
            [myView addSubview:realSavingsSuccessor];
            
            UIButton *btnAddSavingsSuccessor=[[UIButton alloc] initWithFrame:CGRectMake(10, 180, 60, 30)];
            [btnAddSavingsSuccessor setTitle:@"+" forState:UIControlStateNormal];
            btnAddSavingsSuccessor.tag=222203;
            [btnAddSavingsSuccessor addTarget:self
                                       action:@selector(btnAddSuccessorAction:)
                             forControlEvents:UIControlEventTouchUpInside];
            
            setBtbBorder(btnAddSavingsSuccessor);
            [myView addSubview:btnAddSavingsSuccessor];
            
            UITextField * tfSavingsPlanSuccessor=[[UITextField alloc]initWithFrame:CGRectMake(65, 180, self.view.frame.size.width-75, 30)];
            tfSavingsPlanSuccessor.backgroundColor=[UIColor clearColor];
            setBorder(tfSavingsPlanSuccessor);
            tfSavingsPlanSuccessor.tag=340;
            [self setTextAt:tfSavingsPlanSuccessor];
            
            
            tfSavingsPlanSuccessor.delegate=self;
            [tfSavingsPlanSuccessor addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfSavingsPlanSuccessor];
            // add new
            
            UIButton *btnAddNewSavings=[[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-10, 220, 160, 30)];
            [btnAddNewSavings setTitle:@" הוספת תכנית חסכון" forState:UIControlStateNormal];
            [btnAddNewSavings addTarget:self
                                 action:@selector(btnAddNewAction:)
                       forControlEvents:UIControlEventTouchUpInside];
            btnAddNewSavings.tag=333303;
            setBtbBorder(btnAddNewSavings);
            [myView addSubview:btnAddNewSavings];
            
            
            //lable 5
            UILabel *SavingsNotes = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 270, 100, 30)];
            [SavingsNotes setTextAlignment:NSTextAlignmentRight];
            [SavingsNotes setText:@"הערות"];
            SavingsNotes.textColor=[UIColor blackColor];
            [myView addSubview:SavingsNotes];
            
            
            UITextField * tfSavingsNotes=[[UITextField alloc]initWithFrame:CGRectMake(10, 300, self.view.frame.size.width-20, 30)];
            tfSavingsNotes.backgroundColor=[UIColor clearColor];
            setBorder(tfSavingsNotes);
            tfSavingsNotes.tag=350;
            [self setTextAt:tfSavingsNotes];
            
            tfSavingsNotes.delegate=self;
            [tfSavingsNotes addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfSavingsNotes];
        }
        
        [cellInside.contentView addSubview:myView];
        
        return cellInside;
        
        
    }
    //cash where are the cash, successor
    
    if (sectionNO==3) {
        
        UITableViewCell *cellInside=[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:[NSIndexPath indexPathForRow:0 inSection:3]];
        cellInside.selectionStyle=UITableViewCellSelectionStyleNone;
        
        cellInside.textLabel.text = @"";
        UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, 350)];
        //myView.backgroundColor=[UIColor redColor];
        
        if([DataClass getSectionFourView])
        {
            myView=[DataClass getSectionFourView];
        }
        else
        {
            //label 1
            UILabel *cashFrom = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-160, 10, 140, 30)];
            [cashFrom setText:@"היכן מצויים הכספים"];
            setLblBorder(cashFrom);
            [myView addSubview:cashFrom];
            
            UITextField * tfCashFrom=[[UITextField alloc]initWithFrame:CGRectMake(10, 40, self.view.frame.size.width-20, 30)];
            tfCashFrom.backgroundColor=[UIColor clearColor];
            setBorder(tfCashFrom);
            tfCashFrom.tag=410;
            [self setTextAt:tfCashFrom];
            
            tfCashFrom.delegate=self;
            [tfCashFrom addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfCashFrom];
            
            
            //lable 4
            UILabel *CashSuccessor = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 80, 100, 30)];
            [CashSuccessor setText:@"שם היורש/יורשים"];
            setLblBorder(CashSuccessor);
            [myView addSubview:CashSuccessor];
            
            UIButton *btnAddCashSuccessor=[[UIButton alloc] initWithFrame:CGRectMake(10, 110, 60, 30)];
            [btnAddCashSuccessor setTitle:@"+" forState:UIControlStateNormal];
            btnAddCashSuccessor.tag=222204;
            [btnAddCashSuccessor addTarget:self
                                    action:@selector(btnAddSuccessorAction:)
                          forControlEvents:UIControlEventTouchUpInside];
            
            setBtbBorder(btnAddCashSuccessor);
            [myView addSubview:btnAddCashSuccessor];
            
            UITextField * tfCashSuccessor=[[UITextField alloc]initWithFrame:CGRectMake(65, 110, self.view.frame.size.width-75, 30)];
            tfCashSuccessor.backgroundColor=[UIColor clearColor];
            setBorder(tfCashSuccessor);
            tfCashSuccessor.tag=440;
            [self setTextAt:tfCashSuccessor];
            
            tfCashSuccessor.delegate=self;
            [tfCashSuccessor addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfCashSuccessor];
            // add new
            
            UIButton *btnAddNewCash=[[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2, 150, 150, 30)];
            [btnAddNewCash setTitle:@"הוספת חשבון בנק" forState:UIControlStateNormal];
            btnAddNewCash.tag=333304;
            [btnAddNewCash addTarget:self
                              action:@selector(btnAddNewAction:)
                    forControlEvents:UIControlEventTouchUpInside];
            setBtbBorder(btnAddNewCash);
            [myView addSubview:btnAddNewCash];
            
            
            //lable 5
            UILabel *cashNotes = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 180, 100, 30)];
            [cashNotes setText:@"הערות"];
            setLblBorder(cashNotes);
            [myView addSubview:cashNotes];
            
            
            UITextField * tfCashNotes=[[UITextField alloc]initWithFrame:CGRectMake(10, 210, self.view.frame.size.width-20, 30)];
            tfCashNotes.backgroundColor=[UIColor clearColor];
            setBorder(tfCashNotes);
            tfCashNotes.tag=450;
            [self setTextAt:tfCashNotes];
            
            tfCashNotes.delegate=self;
            [tfCashNotes addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfCashNotes];
        }
        
        [cellInside.contentView addSubview:myView];
        return cellInside;
        
        
    }
    
    //company share coma name, number
    if (sectionNO==4) {
        
        UITableViewCell *cellInside=[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:[NSIndexPath indexPathForRow:0 inSection:4]];
        cellInside.selectionStyle=UITableViewCellSelectionStyleNone;
        
        cellInside.textLabel.text = @"";
        
        UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, 350)];
        //myView.backgroundColor=[UIColor redColor];
       
        if([DataClass getSectionFiveView])
        {
            myView=[DataClass getSectionFiveView];
        }
        else
        {
            //label 1
            UILabel *comapnyName = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-200, 10, 180, 30)];
            [comapnyName setText:@"שם חברה (מומלץ להוסיף ח.פ)"];
            setLblBorder(comapnyName);
            [myView addSubview:comapnyName];
            
            UITextField * tfCompanyName=[[UITextField alloc]initWithFrame:CGRectMake(10, 40, self.view.frame.size.width-20, 30)];
            tfCompanyName.backgroundColor=[UIColor clearColor];
            setBorder(tfCompanyName);
            tfCompanyName.tag=510;
            [self setTextAt:tfCompanyName];
            
            tfCompanyName.delegate=self;
            [tfCompanyName addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfCompanyName];
            
            
            //lable 4
            UILabel *companySuccessor = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 80, 100, 30)];
            [companySuccessor setText:@"שם היורש/יורשים"];
            setLblBorder(companySuccessor);
            [myView addSubview:companySuccessor];
            
            UIButton *btnAddCompanySuccessor=[[UIButton alloc] initWithFrame:CGRectMake(10, 110, 60, 30)];
            [btnAddCompanySuccessor setTitle:@"+" forState:UIControlStateNormal];
            btnAddCompanySuccessor.tag=222205;
            [btnAddCompanySuccessor addTarget:self
                                       action:@selector(btnAddSuccessorAction:)
                             forControlEvents:UIControlEventTouchUpInside];
            
            setBtbBorder(btnAddCompanySuccessor);
            
            [myView addSubview:btnAddCompanySuccessor];
            
            UITextField * tfCompanySuccessor=[[UITextField alloc]initWithFrame:CGRectMake(65, 110, self.view.frame.size.width-75, 30)];
            tfCompanySuccessor.backgroundColor=[UIColor clearColor];
            setBorder(tfCompanySuccessor);
            tfCompanySuccessor.tag=540;
            [self setTextAt:tfCompanySuccessor];
            
            tfCompanySuccessor.delegate=self;
            [tfCompanySuccessor addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfCompanySuccessor];
            // add new
            
            UIButton *btnAddNewCompany=[[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2, 150, 150, 30)];
            [btnAddNewCompany setTitle:@"הוספת חשבון בנק" forState:UIControlStateNormal];
            btnAddNewCompany.tag=333305;
            [btnAddNewCompany addTarget:self
                                 action:@selector(btnAddNewAction:)
                       forControlEvents:UIControlEventTouchUpInside];
            setBtbBorder(btnAddNewCompany);
            [myView addSubview:btnAddNewCompany];
            
            
            //lable 5
            UILabel *companyNotes = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 180, 100, 30)];
            [companyNotes setText:@"הערות"];
            setLblBorder(companyNotes);
            [myView addSubview:companyNotes];
            
            
            UITextField * tfCompanyNotes=[[UITextField alloc]initWithFrame:CGRectMake(10, 210, self.view.frame.size.width-20, 30)];
            tfCompanyNotes.backgroundColor=[UIColor clearColor];
            setBorder(tfCompanyNotes);
            tfCompanyNotes.tag=550;
            [self setTextAt:tfCompanyNotes];
            
            tfCompanyNotes.delegate=self;
            [tfCompanyNotes addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfCompanyNotes];
        }
        
        [cellInside.contentView addSubview:myView];
        
        return cellInside;
        
    }
    //security exchange and security bank branch , acc no,
    
    if (sectionNO==5) {
        
        UITableViewCell *cellInside=[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:[NSIndexPath indexPathForRow:0 inSection:5]];
        cellInside.selectionStyle=UITableViewCellSelectionStyleNone;
        
        cellInside.textLabel.text = @"";
        UIView * myView = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, 350)];
        //myView.backgroundColor=[UIColor redColor];
    
        if([DataClass getSectionSixView])
        {
            myView=[DataClass getSectionSixView];
        }
        else
        {
            //label 1
            UILabel *securityBankName = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 10, 100, 30)];
            [securityBankName setText:@"שם הבנק"];
            setLblBorder(securityBankName);
            [myView addSubview:securityBankName];
            
            UITextField * tfSecurityBankName=[[UITextField alloc]initWithFrame:CGRectMake(10, 40, self.view.frame.size.width-20, 30)];
            tfSecurityBankName.backgroundColor=[UIColor clearColor];
            setBorder(tfSecurityBankName);
            tfSecurityBankName.tag=610;
            [self setTextAt:tfSecurityBankName];
            
            tfSecurityBankName.delegate=self;
            [tfSecurityBankName addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfSecurityBankName];
            
            
            //label 2 +3
            UILabel *securityBankAccountNO = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-280, 80, 100, 30)];
            [securityBankAccountNO setText:@"מספר"];
            setLblBorder(securityBankAccountNO);
            [myView addSubview:securityBankAccountNO];
            
            UITextField * tfSecurityBankAccountNO=[[UITextField alloc]initWithFrame:CGRectMake(10, 110, 130, 30)];
            tfSecurityBankAccountNO.backgroundColor=[UIColor clearColor];
            setBorder(tfSecurityBankAccountNO);
            tfSecurityBankAccountNO.tag=620;
            [self setTextAt:tfSecurityBankAccountNO];
            
            tfSecurityBankAccountNO.delegate=self;
            [tfSecurityBankAccountNO addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfSecurityBankAccountNO];
            
            UILabel *securityBranchName = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 80, 100, 30)];//(self.view.frame.size.width-120, 80, 100, 30)];
            [securityBranchName setText:@"סניף"];
            setLblBorder(securityBranchName);
            [myView addSubview:securityBranchName];
            
            UITextField * tfSecurityBranchName=[[UITextField alloc]initWithFrame:CGRectMake(180, 110, 130, 30)];
            tfSecurityBranchName.backgroundColor=[UIColor clearColor];
            setBorder(tfSecurityBranchName);
            tfSecurityBranchName.tag=630;
            [self setTextAt:tfSecurityBranchName];
            
            tfSecurityBranchName.delegate=self;
            [tfSecurityBranchName addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfSecurityBranchName];
            
            
            //lable 4
            UILabel *securitySuccessor = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 150, 100, 30)];
            [securitySuccessor setText:@"שם היורש/יורשים"];
            setLblBorder(securitySuccessor);
            [myView addSubview:securitySuccessor];
            
            UIButton *btnAddSecuritySuccessor=[[UIButton alloc] initWithFrame:CGRectMake(10, 180, 60, 30)];
            [btnAddSecuritySuccessor setTitle:@"+" forState:UIControlStateNormal];
            btnAddSecuritySuccessor.tag=222206;
            [btnAddSecuritySuccessor addTarget:self
                                        action:@selector(btnAddSuccessorAction:)
                              forControlEvents:UIControlEventTouchUpInside];
            
            setBtbBorder(btnAddSecuritySuccessor);
            [myView addSubview:btnAddSecuritySuccessor];
            
            UITextField * tfSecuritySuccessor=[[UITextField alloc]initWithFrame:CGRectMake(65, 180, self.view.frame.size.width-75, 30)];
            tfSecuritySuccessor.backgroundColor=[UIColor clearColor];
            setBorder(tfSecuritySuccessor);
            tfSecuritySuccessor.tag=640;
            [self setTextAt:tfSecuritySuccessor];
            
            tfSecuritySuccessor.delegate=self;
            [tfSecuritySuccessor addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfSecuritySuccessor];
            // add new
            
            UIButton *btnAddNewSecurity=[[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-40, 220, 190, 30)];
            [btnAddNewSecurity setTitle:@"הוספת ניירות ערך ומט\"ח" forState:UIControlStateNormal];
            btnAddNewSecurity.tag=333306;
            [btnAddNewSecurity addTarget:self
                                  action:@selector(btnAddNewAction:)
                        forControlEvents:UIControlEventTouchUpInside];
            setBtbBorder(btnAddNewSecurity);
            [myView addSubview:btnAddNewSecurity];
            
            
            //lable 5
            UILabel *securityNotes = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 270, 100, 30)];
            [securityNotes setText:@"הערות"];
            setLblBorder(securityNotes);
            [myView addSubview:securityNotes];
            
            
            UITextField * tfSecurityNotes=[[UITextField alloc]initWithFrame:CGRectMake(10, 300, self.view.frame.size.width-20, 30)];
            tfSecurityNotes.backgroundColor=[UIColor clearColor];
            setBorder(tfSecurityNotes);
            tfSecurityNotes.tag=650;
            [self setTextAt:tfSecurityNotes];
            
            tfSecurityNotes.delegate=self;
            [tfSecurityNotes addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfSecurityNotes];
        }
        
        
        [cellInside.contentView addSubview:myView];
    
        return cellInside;
        
        
    }
    
    // jewelary details
    if (sectionNO==6) {
        
        UITableViewCell *cellInside=[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:[NSIndexPath indexPathForRow:0 inSection:6]];
        cellInside.selectionStyle=UITableViewCellSelectionStyleNone;
        
        cellInside.textLabel.text = @"";
        UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, 350)];
        //myView.backgroundColor=[UIColor redColor];
      
        if([DataClass getSectionSevenView])
        {
            myView=[DataClass getSectionSevenView];
        }
        else
        {
            //label 1
            UILabel *JewelryDetail = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 10, 100, 30)];
            [JewelryDetail setText:@"פירוט"];
            setLblBorder(JewelryDetail);
            [myView addSubview:JewelryDetail];
            
            UITextField * tfJewelryDetail =[[UITextField alloc]initWithFrame:CGRectMake(10, 40, self.view.frame.size.width-20, 30)];
            tfJewelryDetail.backgroundColor=[UIColor clearColor];
            setBorder(tfJewelryDetail);
            tfJewelryDetail.tag=710;
            [self setTextAt:tfJewelryDetail];
            
            tfJewelryDetail.delegate=self;
            [tfJewelryDetail addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfJewelryDetail];
            
            
            //lable 4
            UILabel * jewelrySuccessor = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 80, 100, 30)];
            [jewelrySuccessor setText:@"שם היורש/יורשים"];
            setLblBorder(jewelrySuccessor);
            [myView addSubview:jewelrySuccessor];
            
            UIButton *btnAddJewelrySuccessor=[[UIButton alloc] initWithFrame:CGRectMake(10, 110, 60, 30)];
            [btnAddJewelrySuccessor setTitle:@"+" forState:UIControlStateNormal];
            btnAddJewelrySuccessor.tag=222207;
            [btnAddJewelrySuccessor addTarget:self
                                       action:@selector(btnAddSuccessorAction:)
                             forControlEvents:UIControlEventTouchUpInside];
            
            setBtbBorder(btnAddJewelrySuccessor);
            [myView addSubview:btnAddJewelrySuccessor];
            
            UITextField * tfJewelrySuccessor=[[UITextField alloc]initWithFrame:CGRectMake(65, 110, self.view.frame.size.width-75, 30)];
            tfJewelrySuccessor.backgroundColor=[UIColor clearColor];
            setBorder(tfJewelrySuccessor);
            tfJewelrySuccessor.tag=740;
            [self setTextAt:tfJewelrySuccessor];
            
            tfJewelrySuccessor.delegate=self;
            [tfJewelrySuccessor addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfJewelrySuccessor];
            // add new
            
            UIButton *btnAddNewJewelry=[[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2, 150, 150, 30)];
            [btnAddNewJewelry setTitle:@" הוספת תכשיטים" forState:UIControlStateNormal];
            btnAddNewJewelry.tag=333307;
            [btnAddNewJewelry addTarget:self
                                 action:@selector(btnAddNewAction:)
                       forControlEvents:UIControlEventTouchUpInside];
            setBtbBorder(btnAddNewJewelry);
            [myView addSubview:btnAddNewJewelry];
        }
        
        
        [cellInside.contentView addSubview:myView];
        
        return cellInside;
    }
    
    // movable properti details
    if (sectionNO==7) {
        
        UITableViewCell *cellInside=[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:[NSIndexPath indexPathForRow:0 inSection:7]];
        cellInside.selectionStyle=UITableViewCellSelectionStyleNone;
        
        cellInside.textLabel.text = @"";
        
        UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, 350)];
        //myView.backgroundColor=[UIColor redColor];
        
        if([DataClass getSectionEightView])
        {
            myView=[DataClass getSectionEightView];
        }
        else
        {
            //label 1
            UILabel *movePropertyDetail = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 10, 100, 30)];
            [movePropertyDetail setText:@"פירוט"];
            setLblBorder(movePropertyDetail);
            
            [myView addSubview:movePropertyDetail];
            
            UITextField * tfMovePropertyDetail =[[UITextField alloc]initWithFrame:CGRectMake(10, 40, self.view.frame.size.width-20, 30)];
            tfMovePropertyDetail.backgroundColor=[UIColor clearColor];
            setBorder(tfMovePropertyDetail);
            tfMovePropertyDetail.tag=810;
            [self setTextAt:tfMovePropertyDetail];
            
            tfMovePropertyDetail.delegate=self;
            [tfMovePropertyDetail addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfMovePropertyDetail];
            
            
            //lable 4
            UILabel * movePropertySuccessor = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 80, 100, 30)];
            [movePropertySuccessor setText:@"שם היורש/יורשים"];
            setLblBorder(movePropertySuccessor);
            [myView addSubview:movePropertySuccessor];
            
            UIButton *btnAddMovePropertySuccessor=[[UIButton alloc] initWithFrame:CGRectMake(10, 110, 60, 30)];
            [btnAddMovePropertySuccessor setTitle:@"+" forState:UIControlStateNormal];
            btnAddMovePropertySuccessor.tag=222208;
            [btnAddMovePropertySuccessor addTarget:self
                                            action:@selector(btnAddSuccessorAction:)
                                  forControlEvents:UIControlEventTouchUpInside];
            
            setBtbBorder(btnAddMovePropertySuccessor);
            [myView addSubview:btnAddMovePropertySuccessor];
            
            UITextField * tfMovePropertySuccessor=[[UITextField alloc]initWithFrame:CGRectMake(65, 110, self.view.frame.size.width-75, 30)];
            tfMovePropertySuccessor.backgroundColor=[UIColor clearColor];
            setBorder(tfMovePropertySuccessor);
            tfMovePropertySuccessor.tag=840;
            [self setTextAt:tfMovePropertySuccessor];
            
            tfMovePropertySuccessor.delegate=self;
            [tfMovePropertySuccessor addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfMovePropertySuccessor];
            // add new
            
            UIButton *btnAddNewMoveProperty=[[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2, 150, 150, 30)];
            [btnAddNewMoveProperty setTitle:@"הוספת מטלטלין " forState:UIControlStateNormal];
            btnAddNewMoveProperty.tag=333308;
            [btnAddNewMoveProperty addTarget:self
                                      action:@selector(btnAddNewAction:)
                            forControlEvents:UIControlEventTouchUpInside];
            setBtbBorder(btnAddNewMoveProperty);
            [myView addSubview:btnAddNewMoveProperty];
        }
        
        [cellInside.contentView addSubview:myView];
        return cellInside;
        
    }
    // Shares on the stock the company
    if (sectionNO==8) {
        
        UITableViewCell *cellInside=[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:[NSIndexPath indexPathForRow:0 inSection:8]];
        cellInside.selectionStyle=UITableViewCellSelectionStyleNone;
        
        cellInside.textLabel.text = @"";
        
        UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, 350)];
        //myView.backgroundColor=[UIColor redColor];
       
        if([DataClass getSectionNineView])
        {
            myView=[DataClass getSectionNineView];
        }
        else
        {
            //label 1
            UILabel *shareCompanyName = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-220, 10, 200, 30)];
            [shareCompanyName setText:@"חברה\\בנק המנהלים את החשבון"];
            setLblBorder(shareCompanyName);
            [myView addSubview:shareCompanyName];
            
            UITextField * tfShareCompanyName=[[UITextField alloc]initWithFrame:CGRectMake(10, 40, self.view.frame.size.width-20, 30)];
            tfShareCompanyName.backgroundColor=[UIColor clearColor];
            setBorder(tfShareCompanyName);
            tfShareCompanyName.tag=910;
            [self setTextAt:tfShareCompanyName];
            
            tfShareCompanyName.delegate=self;
            [tfShareCompanyName addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfShareCompanyName];
            
            
            //lable 4
            UILabel *ShareSuccessor = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 80, 100, 30)];
            [ShareSuccessor setText:@"שם היורש/יורשים"];
            setLblBorder(ShareSuccessor);
            [myView addSubview:ShareSuccessor];
            
            UIButton *btnAddShareSuccessor=[[UIButton alloc] initWithFrame:CGRectMake(10, 110, 60, 30)];
            [btnAddShareSuccessor setTitle:@"+" forState:UIControlStateNormal];
            btnAddShareSuccessor.tag=222209;
            [btnAddShareSuccessor addTarget:self
                                     action:@selector(btnAddSuccessorAction:)
                           forControlEvents:UIControlEventTouchUpInside];
            
            setBtbBorder(btnAddShareSuccessor);
            [myView addSubview:btnAddShareSuccessor];
            
            UITextField * tfShareSuccessor=[[UITextField alloc]initWithFrame:CGRectMake(65, 110, self.view.frame.size.width-75, 30)];
            tfShareSuccessor.backgroundColor=[UIColor clearColor];
            setBorder(tfShareSuccessor);
            tfShareSuccessor.tag=940;
            [self setTextAt:tfShareSuccessor];
            
            tfShareSuccessor.delegate=self;
            [tfShareSuccessor addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfShareSuccessor];
            // add new
            
            UIButton *btnAddNewShare=[[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2, 150, 150, 30)];
            [btnAddNewShare setTitle:@" הוספת מניות בבורסה" forState:UIControlStateNormal];
            btnAddNewShare.tag=333309;
            [btnAddNewShare addTarget:self
                               action:@selector(btnAddNewAction:)
                     forControlEvents:UIControlEventTouchUpInside];
            setBtbBorder(btnAddNewShare);
            [myView addSubview:btnAddNewShare];
            
            
            //lable 5
            UILabel *shareNotes = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 180, 100, 30)];
            [shareNotes setText:@"הערות"];
            setLblBorder(shareNotes);
            [myView addSubview:shareNotes];
            
            
            UITextField * tfShareNotes=[[UITextField alloc]initWithFrame:CGRectMake(10, 210, self.view.frame.size.width-20, 30)];
            tfShareNotes.backgroundColor=[UIColor clearColor];
            setBorder(tfShareNotes);
            tfShareNotes.tag=950;
            [self setTextAt:tfShareNotes];
            
            tfShareNotes.delegate=self;
            [tfShareNotes addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfShareNotes];
        }
        
        [cellInside.contentView addSubview:myView];
       
        return cellInside;
        
        
    }
    
    // another property details
    if (sectionNO==9) {
        
        UITableViewCell *cellInside=[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:[NSIndexPath indexPathForRow:0 inSection:9]];
        cellInside.selectionStyle=UITableViewCellSelectionStyleNone;
        
        cellInside.textLabel.text = @"";
        
        UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, 350)];
        //myView.backgroundColor=[UIColor redColor];
        
        if([DataClass getSectionTenView])
        {
            myView=[DataClass getSectionTenView];
        }
        else
        {
            //label 1
            UILabel *anotherPropertyDetail = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 10, 100, 30)];
            [anotherPropertyDetail setText:@"פירוט"];
            setLblBorder(anotherPropertyDetail);
            [myView addSubview:anotherPropertyDetail];
            
            UITextField * tfAnotherPropertyDetail =[[UITextField alloc]initWithFrame:CGRectMake(10, 40, self.view.frame.size.width-20, 30)];
            tfAnotherPropertyDetail.backgroundColor=[UIColor clearColor];
            setBorder(tfAnotherPropertyDetail);
            tfAnotherPropertyDetail.tag=1010;
            [self setTextAt:tfAnotherPropertyDetail];
            
            tfAnotherPropertyDetail.delegate=self;
            [tfAnotherPropertyDetail addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfAnotherPropertyDetail];
            
            
            //lable 4
            UILabel * anotherPropertySuccessor = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-120, 80, 100, 30)];
            [anotherPropertySuccessor setText:@"שם היורש/יורשים"];
            setLblBorder(anotherPropertySuccessor);
            [myView addSubview:anotherPropertySuccessor];
            
            UIButton *btnAddAnotherPropertySuccessor=[[UIButton alloc] initWithFrame:CGRectMake(10, 110, 60, 30)];
            [btnAddAnotherPropertySuccessor setTitle:@"+" forState:UIControlStateNormal];
            btnAddAnotherPropertySuccessor.tag=222210;
            [btnAddAnotherPropertySuccessor addTarget:self
                                               action:@selector(btnAddSuccessorAction:)
                                     forControlEvents:UIControlEventTouchUpInside];
            
            setBtbBorder(btnAddAnotherPropertySuccessor);
            [myView addSubview:btnAddAnotherPropertySuccessor];
            
            UITextField * tfAnotherPropertySuccessor=[[UITextField alloc]initWithFrame:CGRectMake(65, 110, self.view.frame.size.width-75, 30)];
            tfAnotherPropertySuccessor.backgroundColor=[UIColor clearColor];
            setBorder(tfAnotherPropertySuccessor);
            tfAnotherPropertySuccessor.tag=1040;
            [self setTextAt:tfAnotherPropertySuccessor];
            
            tfAnotherPropertySuccessor.delegate=self;
            [tfAnotherPropertySuccessor addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [myView addSubview:tfAnotherPropertySuccessor];
            // add new
            
            UIButton *btnAddNewAnotherProperty=[[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2, 150, 150, 30)];
            [btnAddNewAnotherProperty setTitle:@"הוספת רכוש נוסף" forState:UIControlStateNormal];
            btnAddNewAnotherProperty.tag=333310;
            [btnAddNewAnotherProperty addTarget:self
                                         action:@selector(btnAddNewAction:)
                               forControlEvents:UIControlEventTouchUpInside];
            setBtbBorder(btnAddNewAnotherProperty);
            [myView addSubview:btnAddNewAnotherProperty];

        }
        
        [cellInside.contentView addSubview:myView];
        
        return cellInside;
    }
    [tableView beginUpdates];
    
    [tableView endUpdates];
    NSLog(@"At cellForRowAtIndexPath with section : %ld and cellTExt :",(long)indexPath.section);
    return cell;
}


// number of section in this table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.headers.count;
}

#pragma mark - UITableViewDelegate

//key borad get hide when return button is pressed
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"At to hide the keyboard");
    [textField resignFirstResponder];
    return YES;
}


- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    //    self.tblExpandable.setTextAlignment: NSTextAlignmentRight
    return [self.tblExpandable headerWithTitle:self.headers[section]  totalRows:self.cells.count inSection:section] ;
}

// which row in whch section has been selected or clicked
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"section-%ld, row-%ld", (long)indexPath.section, (long)indexPath.row);
    
}

// returnting the height of each row
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger sectionNo =indexPath.section;
    NSInteger size;
    if (sectionNo==0 || sectionNo==1 || sectionNo==2 || sectionNo==5) {
        size= 350.f;
    }
    else if ( sectionNo==3)
    {
        size=280.0f;
    }
    else{
        size = 300.0f;
    }
    return size;
    
}

@end
