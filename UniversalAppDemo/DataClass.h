//
//  DataClass.h
//  MakeWill
//
//  Created by Cor2Tect on 4/13/16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataClass : NSObject


+(void)setDataDic:(NSMutableDictionary*)dic;
+(NSMutableDictionary *)getDataDic;
//owner info


+(void)setOwnerName:(NSString*)value;
+(NSString*)getOwnerName;

+(void)setOwnerIdCardNo:(NSString*)value;
+(NSString*)getOwnerIdCardNo;


+(void)setOwnerAddress:(NSString*)value;
+(NSString*)getOwnerAddress;


+(void)setOwnerPhoneNo:(NSString*)value;
+(NSString*)getOwnerPhoneNo;


+(void)setSingleSuccessorName:(NSString*)value;
+(NSString*)getSingleSuccessorName;


+(void)setSingleSuccessorStatus:(bool)value;
+(bool)getSingleSuccessorStatus;


//end owner Info

+(void)setVideopath:(NSURL*)value;
+(NSURL *)getVideoPath;

+(void)setImagePath:(NSString *)path;
+(NSString *)getImagePath;

+(void)setPersonName:(NSString *)name;
+(NSString *)getPersonName;

+(void)setRowNumber:(NSInteger)rowNumber;
+(NSInteger)getRowNUmber;

+(void)setTableView:(UITableView*)tableView;
+(UITableView*)getTable;


// mail body
+(void)setMailBody:(NSString *)value;
+(NSString *)getmailBody;

// all the saved View

+(void)setSectionOneView : (UIView *)valueView;
+(UIView*)getSectionOneView;

+(void)setSectionTwoView : (UIView *)valueView;
+(UIView*)getSectionTwoView;

+(void)setSectionThreeView : (UIView *)valueView;
+(UIView*)getSectionThreeView;

+(void)setSectionFourView : (UIView *)valueView;
+(UIView*)getSectionFourView;

+(void)setSectionFiveView : (UIView *)valueView;
+(UIView*)getSectionFiveView;

+(void)setSectionSixView : (UIView *)valueView;
+(UIView*)getSectionSixView;

+(void)setSectionSevenView : (UIView *)valueView;
+(UIView*)getSectionSevenView;

+(void)setSectionEightView : (UIView *)valueView;
+(UIView*)getSectionEightView;

+(void)setSectionNineView : (UIView *)valueView;
+(UIView*)getSectionNineView;

+(void)setSectionTenView : (UIView *)valueView;
+(UIView*)getSectionTenView;


@end
