//
//  aboutFirmViewController.m
//  MakeWill
//
//  Created by Cor2Tect on 4/9/16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

#import "aboutFirmViewController.h"

@interface aboutFirmViewController ()

@end

@implementation aboutFirmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    
//    UIScrollView *scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 270, self.view.frame.size.width, self.view.frame.size.height)];
//
//    scrollview.contentSize = CGSizeMake(self.view.frame.size.width, 900);
//    [self.view addSubview:scrollview];
//    
//   //_scrollDetails.
//    UITextView *firstTestView = [[UITextView alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-20, 150)];
//    [firstTestView setTextAlignment:NSTextAlignmentRight];
//    [firstTestView setBackgroundColor:[UIColor clearColor]];
//    firstTestView.editable=NO;
//    [firstTestView setText:@"ממשרד עורכי דין חדוה וייס הוקם בשנת 2006  ומתמחה בתחום דיני המשפחה והירושה, כמו כן המשרד מטפל בתיקי גירושין, לרבות תיקי משמורת, מזונות ורכוש, עריכת הסכמי ממון והסכמי טרום נישואין."];
//    firstTestView.textColor=[UIColor blackColor];
//    firstTestView.scrollEnabled = NO;
//    [scrollview addSubview:firstTestView];
//    
//    UITextView *textView2 = [[UITextView alloc]  initWithFrame:CGRectMake(10.0f, 50.0f, self.view.frame.size.width-20, 100.0f)];
//    textView2.text=@"צוואות ובהגשת בקשות לצו קיום צוואה וכן לצו ירושה לרשם לענייני ירושה.עורכת דין חדוה וייס משמשת כיום כחברת בית דין משמעתי של לשכת עורכי הדין בישראל,";
//    textView2.textAlignment=NSTextAlignmentRight;
//    textView2.textColor=[UIColor blackColor];
//    textView2.scrollEnabled = NO;
//    textView2.editable=NO;
//
//    [scrollview addSubview:textView2];
//    
//    UITextView *textView3 = [[UITextView alloc]  initWithFrame:CGRectMake(10.0f, 90.0f, self.view.frame.size.width-20, 100.0f)];
//    textView3.text=@"במסגרת תפקידה משמשת כדיינית בבית הדין המשמעתי, בקבילות המוגשות כנגד עורכי דין.כמו כן עו\"ד חדוה וייס הנה מגשרת מוסמכת מטעם לשכת עורכי הדין,  ומגשרת בדיני משפחה בפרט, מטעם לשכת עורכי הדין.";
//    textView3.textAlignment=NSTextAlignmentRight;
//    textView3.textColor=[UIColor blackColor];
//    textView3.font=[UIFont boldSystemFontOfSize:14.0f];
//    textView3.scrollEnabled = NO;
//    textView3.editable=NO;
//    
//    [scrollview addSubview:textView3];
//    
//    
//    UITextView *textView4 = [[UITextView alloc]  initWithFrame:CGRectMake(10.0f, 130.0f, self.view.frame.size.width-20, 100.0f)];
//    textView4.text=@" אשמח להגיע לפגישה בחברתכם לשם מתן מידע ופרטים נוספים.";
//    textView4.textAlignment=NSTextAlignmentRight;
//    textView4.textColor=[UIColor blackColor];
//    textView4.scrollEnabled = NO;
//    textView4.editable=NO;
//    
//    [scrollview addSubview:textView4];
//    
//    //1st redLabel
//    UILabel *label1 = [[UILabel alloc]  initWithFrame:CGRectMake(10.0f, 160.0f, self.view.frame.size.width-20, 100.0f)];
//    label1.text=@"המטרה שלנו";
//    label1.textAlignment=NSTextAlignmentRight;
//    label1.textColor=[UIColor redColor];
//    label1.font=[UIFont boldSystemFontOfSize:14.0f];
//    
//    [scrollview addSubview:label1];
//    
//    UITextView *textView5 = [[UITextView alloc]  initWithFrame:CGRectMake(10.0f, 220.0f, self.view.frame.size.width-20, 100.0f)];
//    textView5.text=@"לאור העובדה כי רוב האנשים בפרט שכירים חושבים כי עשיית צוואה כרוכה בכסף רב ובזמן  ומאחר משרדנו מתמחה בתחום עריכת צוואות הננו מתכבדים להציע לעובדי חברתכם עסקת חבילה שאנו נגיע אליהם במרוכז והליך עשיית הצוואה יהא מהיר ונוח.";
//    textView5.textAlignment=NSTextAlignmentRight;
//    textView5.textColor=[UIColor blackColor];
//    textView5.scrollEnabled = NO;
//    textView5.editable=NO;
//    textView5.font=[UIFont boldSystemFontOfSize:14.0f];
//    
//    [scrollview addSubview:textView5];
//    
//    
//    //2nd red label
//    
//    UILabel *label2 = [[UILabel alloc]  initWithFrame:CGRectMake(10.0f, 300.0f, self.view.frame.size.width-20, 100.0f)];
//    label2.text=@"חשיבותה של עריכת צוואה:";
//    label2.textAlignment=NSTextAlignmentRight;
//    label2.textColor=[UIColor redColor];
//    label2.font=[UIFont boldSystemFontOfSize:14.0f];
//    
//    [scrollview addSubview:label2];
//    
//    UITextView *textView6 = [[UITextView alloc]  initWithFrame:CGRectMake(10.0f, 370.0f, self.view.frame.size.width-20, 100.0f)];
//    textView6.text=@"לפי סעיף 2 לחוק הירושה, הירושה מתחלקת על פי דין, זולת אם נערכה צוואה.הלכה למעשה ירושה על פי דין, הינה ברירת מחדל אשר חלה כאשר המוריש לא הותיר אחריו צוואה.בשונה מבעבר, כיום ידוע לכל, כי עריכת צוואה אינה נחלתם של עשירים בלבד, אלא היא רלוונטית מאוד ואף נחוצה לכל אדם אשר ברשותו ו/או בבעלותו רכוש מסוים, חשבונות בנק, נכסי נדל\"ן, קופות גמל, קרנות השתלמות וכיוצ\"ב.עריכת צוואה לא אחת מונעת סכסוכים ומחלוקות, שכן קיומה של צוואה הקובעת באופן ברור, שאינו משמע לשתי פנים, את אופן חלוקת הרכוש, מונעת את מרביתם של סכסוכי ירושה למיניהם.";
//    textView6.textAlignment=NSTextAlignmentRight;
//    textView6.textColor=[UIColor blackColor];
//    textView6.scrollEnabled = NO;
//    textView6.editable=NO;
//    //textView6.font=[UIFont boldSystemFontOfSize:14.0f];
//    
//    [scrollview addSubview:textView6];
//    
//    //3rd label
//    
//    //2nd red label
//    
//    UILabel *label3 = [[UILabel alloc]  initWithFrame:CGRectMake(10.0f, 440.0f, self.view.frame.size.width-20, 100.0f)];
//    label3.text=@"מטרתה של הצוואה:";
//    label3.textAlignment=NSTextAlignmentRight;
//    label3.textColor=[UIColor redColor];
//    label3.font=[UIFont boldSystemFontOfSize:14.0f];
//    
//    [scrollview addSubview:label3];
//    
//    UITextView *textView7 = [[UITextView alloc]  initWithFrame:CGRectMake(10.0f, 510.0f, self.view.frame.size.width-20, 100.0f)];
//    textView7.text=@"הינה להבטיח כי רכושו של המצווה יחולק לאחר מותו, בהתאם לרצונו האחרון כפי שהעלה על הכתב, בדיוק באופן שהוא רוצה בו.על פי החוק צוואה הינה דרך שבה אדם מגדיר מה יעשה ברכושו ובנכסיו לאחר מותו.החוק קובע כי כל אדם רשאי לצוות את רכושו ונכסיו, לטובת כל מי שהוא חפץ, וכן לכלול בצוואה הוראות לגבי אופן חלוקת נכסי העיזבון.צוואה הינה מעין חוזה בין המוריש ליורשיו, כאשר לאחר מותו של המוריש, המדינה היא שאוכפת את קיומו של החוזה, ומגינה על זכויות היורש עפ\"י הצוואה.";
//    textView7.textAlignment=NSTextAlignmentRight;
//    textView7.textColor=[UIColor blackColor];
//    textView7.scrollEnabled = NO;
//    textView7.editable=NO;
//    //textView7.font=[UIFont boldSystemFontOfSize:14.0f];
//    
//    [scrollview addSubview:textView7];
//
//    

    

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
