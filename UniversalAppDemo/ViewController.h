//
//  ViewController.h
//  UniversalAppDemo
//
//  Created by Simon on 17/10/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property(strong ,nonatomic)IBOutlet UIStoryboard *mainStoryboard;
@property(strong ,nonatomic)IBOutlet UIViewController *vc;

- (IBAction)btnPrepareWill:(id)sender;

- (IBAction)btnprepWill:(id)sender;

- (IBAction)btnAboutFirm:(id)sender;
- (IBAction)btnImageAbout:(id)sender;
- (IBAction)btnTerms:(id)sender;
- (IBAction)btnImageterms:(id)sender;

@end
