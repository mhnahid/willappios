//
//  DataClass.m
//  MakeWill
//
//  Created by Cor2Tect on 4/13/16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

#import "DataClass.h"

@implementation DataClass

static NSMutableDictionary * dataDic;//=[NSDictionary dictionary];

static NSString * personName;
static NSString * imagePath;
static NSInteger  row;
static UITableView * tab;

static NSURL * videoPath;


+(void)setDataDic:(NSMutableDictionary*)dic
{
    dataDic=dic;
}
+(NSMutableDictionary *)getDataDic
{
    return dataDic;
}

//Owner Info
static bool singleSuccessorStatus;

static NSString * ownerName;
static NSString * ownerIdCardNo;
static NSString * ownerAddress;
static NSString * ownerPhoneNo;
static NSString * singleSuccessorName;

+(void)setOwnerName:(NSString*)value
{
    ownerName=value;
}
+(NSString*)getOwnerName
{
    return ownerName;
}


+(void)setOwnerIdCardNo:(NSString*)value
{
    ownerIdCardNo=value;
}
+(NSString*)getOwnerIdCardNo
{
    return ownerIdCardNo;
}


+(void)setOwnerAddress:(NSString*)value
{
    ownerAddress=value;
}
+(NSString*)getOwnerAddress
{
    return ownerAddress;
}


+(void)setOwnerPhoneNo:(NSString*)value
{
    ownerPhoneNo=value;
}
+(NSString*)getOwnerPhoneNo
{
    return ownerPhoneNo;
}


+(void)setSingleSuccessorName:(NSString*)value
{
    singleSuccessorName=value;
}
+(NSString*)getSingleSuccessorName
{
    return singleSuccessorName;
}


+(void)setSingleSuccessorStatus:(bool)value
{
    singleSuccessorStatus=value;
}
+(bool)getSingleSuccessorStatus
{
    return singleSuccessorStatus;
}
//end owner


+(void)setVideopath:(NSURL*)value
{
    videoPath=value;
}
+(NSURL *)getVideoPath
{
    return videoPath;
}

+(void)setImagePath:(NSString *)path
{
    imagePath=path;
}
+(NSString *)getImagePath
{
    return imagePath;
}

+(void)setPersonName:(NSString *)name
{
    personName=name;
}
+(NSString *)getPersonName
{
    return personName;
}

+(void)setRowNumber:(NSInteger)rowNumber
{
    row=rowNumber;
}
+(NSInteger)getRowNUmber
{
    return row;
}

+(void)setTableView:(UITableView*)tableView
{
    tab=tableView;
}
+(UITableView*)getTable
{
    return tab;
}

static NSString * mainBody;
// mail body
+(void)setMailBody:(NSString *)value
{
    mainBody=value;
}
+(NSString *)getmailBody
{
    return mainBody;
}

// view Implementation
static UIView * sectionOne;
static UIView * sectionTwo;
static UIView * sectionThree;
static UIView * sectionFour;
static UIView * sectionFive;
static UIView * sectionSix;
static UIView * sectionSeven;
static UIView * sectionEight;
static UIView * sectionNine;
static UIView * sectionTen;




+(void)setSectionOneView : (UIView *)valueView
{
    sectionOne=valueView;
}

+(UIView*)getSectionOneView
{
    return sectionOne;
}

+(void)setSectionTwoView : (UIView *)valueView
{
    sectionTwo=valueView;
}
+(UIView*)getSectionTwoView
{
    return sectionTwo;
}

+(void)setSectionThreeView : (UIView *)valueView
{
    sectionThree=valueView;
}
+(UIView*)getSectionThreeView
{
    return sectionThree;
}

+(void)setSectionFourView : (UIView *)valueView
{
    sectionFour=valueView;
}
+(UIView*)getSectionFourView
{
    return sectionFour;
}

+(void)setSectionFiveView : (UIView *)valueView
{
    sectionFive=valueView;
}
+(UIView*)getSectionFiveView
{
    return sectionFive;
}

+(void)setSectionSixView : (UIView *)valueView
{
    sectionSix=valueView;
}
+(UIView*)getSectionSixView
{
    return sectionSix;
}

+(void)setSectionSevenView : (UIView *)valueView
{
    sectionSeven=valueView;
}
+(UIView*)getSectionSevenView
{
    return sectionSeven;
}

+(void)setSectionEightView : (UIView *)valueView
{
    sectionEight=valueView;
}
+(UIView*)getSectionEightView
{
    return  sectionEight;
}

+(void)setSectionNineView : (UIView *)valueView
{
    sectionNine =  valueView;
}
+(UIView*)getSectionNineView
{
    return sectionNine;
}

+(void)setSectionTenView : (UIView *)valueView
{
    sectionTen = valueView;
}
+(UIView*)getSectionTenView
{
    return  sectionTen;
}

@end
