//
//  PaymentViewController.h
//  MakeWill
//
//  Created by Cor2Tect on 4/13/16.
//  Copyright © 2016 Appcoda. All rights reserved.
//
#import <MessageUI/MessageUI.h>
#import <UIKit/UIKit.h>
#import "UIView+Toast.h"


@interface PaymentViewController  : UIViewController<MFMailComposeViewControllerDelegate>
{
MFMailComposeViewController *mailComposer;
}

@property (strong, nonatomic) IBOutlet UITextField *tfPaymentName;

//@property (strong, nonatomic) IBOutlet UITextField *tfPaymentEmail;
//@property (strong, nonatomic) IBOutlet UITextField *tfPaymentPhone;

@property (strong, nonatomic) IBOutlet UITextField *tfPaymentCardNo;
@property (strong, nonatomic) IBOutlet UITextField *tfPaymentId;
@property (strong, nonatomic) IBOutlet UITextField *tfPaymentValMonth;
@property (strong, nonatomic) IBOutlet UITextField *tfPaymentValYear;

@property (strong, nonatomic) IBOutlet UITextField *tfPaymentCCv;
@property (strong, nonatomic) IBOutlet UITextField *tfPaymentAmount;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *aiPaymentProgress;

@property (strong, nonatomic) IBOutlet UITextField *activeField;

@property (strong, nonatomic) IBOutlet UIButton *btnPayment;

@property (strong, nonatomic) IBOutlet UIWebView *viewPayment;
- (IBAction)btnLoad:(id)sender;
-(void)makePayment;
//+(NSArray *)arrayOfContacts;
@end
